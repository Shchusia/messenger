from flask import Blueprint,\
    request, \
    jsonify

import binascii
import os
import traceback
from api.list_api import api_update_profile,\
    api_profile
from api._api_answers import error_arguments, \
    error_server,\
    error_connect,\
    good_select,\
    error_select
from api.config_api import create_connection
from config import str_path_to_photo_profile,\
    MEDIA_FOLDER
from api._models.User import User

from api.__init__ import app,\
    checker_valid_data_user
from api.profile.db_profile import update_user,\
    update_password_hash,\
    insert_update_push_data,\
    save_photo_user
import os.path

from api.redis_db.redb import remove_user,\
    set_user_multi

from socket_server.redis_cache import set_name_user

MAX_FILE_SIZE = 1024 * 1024 + 1


@checker_valid_data_user
def update_profile():
    # сделать загрузку фото
    try:
        id_user = request.headers['Id-User']
        conn = create_connection()
        # print(conn)
        full_name = None
        phone = None
        email = None
        about_me = None
        birthday = None
        is_male = None
        password = None
        is_ios = None
        device_id = None
        push_key = None
        session_key = request.headers['Session-Key']
        try:
            full_name = request.json['full_name']
            set_name_user(id_user, full_name)
        except:
            pass

        try:
            phone = request.json['phone']
        except:
            pass

        try:
            email = request.json['email']
        except:
            pass

        try:
            about_me = request.json['about_me']
        except:
            pass

        try:
            birthday = request.json['birthday']
        except:
            pass

        try:
            is_male = request.json['is_male']
        except:
            pass

        try:
            password = request.json['password']
        except:
            pass

        try:
            is_ios = request.json['is_ios']
            device_id = request.json['device_id']
            push_key = request.json['push_key']
        except:
            pass
        try:
            if not os.path.exists(MEDIA_FOLDER + str_path_to_photo_profile):
                os.makedirs(MEDIA_FOLDER + str_path_to_photo_profile)
            file = request.files.get('file')
            name = (str(binascii.hexlify(os.urandom(20)))[2:42]) + '.{}'.format(file.filename.split('.')[-1])
            out = open(MEDIA_FOLDER + str_path_to_photo_profile + name, 'wb')
            while True:
                file_bytes = file.read(MAX_FILE_SIZE)
                out.write(file_bytes)
                if len(file_bytes) < MAX_FILE_SIZE:
                    break
            out.close()
            conn = create_connection()
            if conn:
                cur = conn.cursor()
                ans = save_photo_user(id_user, name, cur)
                if ans > 0:
                    conn.commit()
                    user = User()
                    data = user.get_profile(id_user, cur)
                    conn.close()
                    return jsonify({
                        'code': 1,
                        'message': good_select,
                        'data': {
                            'profile': data
                        }
                    })
                return jsonify({
                        'code': 0,
                        'message': error_select,
                        'data': {}
                    })
            return jsonify({
                    'code': 0,
                    'message': error_connect,
                    'data': {}
                })

        except:
            traceback.print_exc()
            # print('not file')
            pass

        if conn:
            try:
                cur = conn.cursor()
                ans = update_user(id_user,
                                  cur,
                                  full_name,
                                  phone,
                                  email,
                                  about_me,
                                  birthday,
                                  is_male)
                if ans == 0:
                    conn.close()
                    if full_name:
                        set_name_user(id_user, full_name)
                    return jsonify({
                        'code': 0,
                        'message': error_select,
                        'data': {}
                    })
                else:
                    user = User()
                    data = user.get_profile(id_user, cur)
                    answer = jsonify({
                        'code': 1,
                        'message': good_select,
                        'data': {
                            'profile': data
                        }
                    })
                    conn.commit()
                    if password:
                        ans_up = update_password_hash(id_user,
                                                      password,
                                                      cur)
                        if ans_up == 0:
                            conn.close()
                            return jsonify({
                                'code': 0,
                                'message': error_select,
                                'data': {}
                            })
                        else:
                            conn.commit()
                            conn.close()
                            remove_user(id_user,
                                        request.headers['Role'])

                            session_key = user.create_session_key(password)
                            set_user_multi(id_user, session_key)
                            answer.headers.extend({
                                'Id-User': str(id_user),
                                'Session-Key': str(session_key),
                                'Role': request.headers['Role']
                            })
                            # изменяем сессию и удалям все текущие
                            pass
                    if is_ios:
                        ans = insert_update_push_data(id_user,
                                                      is_ios,
                                                      push_key,
                                                      device_id,
                                                      session_key)
                        if ans > 0:
                            return answer
                        elif ans == 0:
                            return jsonify({
                                'code': 0,
                                'message': error_connect,
                                'data': {}
                            })
                        else:
                            return jsonify({
                                'code': 0,
                                'message': error_select,
                                'data': {}
                            })
                    return answer
            except:
                return jsonify({
                    'code': 0,
                    'message': error_select,
                    'data': {}
                })
        # print('sadas')
        return jsonify({
            'code': 0,
            'message': error_connect,
            'data': {}
        })
    except KeyError:
        return jsonify({
            'code': 0,
            'message': error_arguments,
            'data': {}
        })
    except:
        traceback.print_exc()
        return jsonify({
            'code': 0,
            'message': error_server,
            'data': {}
        })


@checker_valid_data_user
def get_profile():
    try:
        # print('hello')
        id_user = request.headers['Id-User']
        conn = create_connection()
        if conn:
            try:
                cur = conn.cursor()
                us = User()
                profile = us.get_profile(id_user, cur)
                conn.close()
                return jsonify({
                    'code': 1,
                    'message': good_select,
                    'data': {
                        'profile': profile
                    }
                })

            except:
                traceback.print_exc()
                return jsonify({
                    'code': 0,
                    'message': error_select,
                    'data': {}
                })
        return jsonify({
            'code': 0,
            'message': error_connect,
            'data': {}
        })
    except KeyError:
        return jsonify({
            'code': 0,
            'message': error_arguments,
            'data': {}
        })
    except:
        traceback.print_exc()
        return jsonify({
            'code': 0,
            'message': error_server,
            'data': {}
        })


app.add_url_rule(api_profile,
                 'profile',
                 get_profile,
                 methods=['GET'])
app.add_url_rule(api_update_profile,
                 'update_profile',
                 update_profile,
                 methods=['POST'])
