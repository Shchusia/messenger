from api.config_api import create_connection
import traceback


def update_user(id_user,
                cur,
                full_name=None,
                phone=None,
                email=None,
                about_me=None,
                birthday=None,
                is_male=None):
    val = []
    if full_name is not None:
        val.append("full_name = '{}' ".format(full_name))
    if phone is not None:
        val.append("phone = '{}' ".format(phone))
    if email is not None:
        val.append("email = '{}' ".format(email))
    if about_me is not None:
        val.append("about_me = '{}' ".format(about_me))
    if birthday is not None:
        val.append("birthday = '{}' ".format(birthday))
    if is_male is not None:
        val.append("is_male = {} ".format(is_male))
    if val:
        select = '''
        UPDATE users
        SET {}
        WHERE id_user = {}
    '''.format(','.join(val), id_user)
        try:
            cur.execute(select)
            return 1
        except:
            traceback.print_exc()
            return 0
    else:
        return 1


def update_password_hash(id_user,
                         password_hash,
                         cur):
    select = '''
            UPDATE users
            SET password_hash = '{}'
            WHERE id_user = {}
        '''.format(password_hash, id_user)
    try:
        cur.execute(select)
        return 1
    except:
        traceback.print_exc()
        return 0


def insert_update_push_data(id_user,
                            is_ios,
                            push_key,
                            device_id,
                            session_key):
    conn = create_connection()
    if conn:
        try:
            cur = conn.cursor()
            select_in = '''
                INSERT INTO push_data (id_push_data, device_id, push_key, is_ios, session_key,ref_id_user)
                VALUES (DEFAULT, '{}', '{}', {}, '{}', {},)
            '''.format(device_id,
                       push_key,
                       is_ios,
                       session_key,
                       id_user)
            cur.execute(select_in)
            conn.commit()
            conn.close()
            return 1
        except:
            # traceback.print_exc()
            try:
                conn.close()
                conn = create_connection()
                cur = conn.cursor()
                select_up = '''
                    UPDATE push_data
                    SET push_key = '{}',
                    is_ios = {},
                    ref_id_user = {},
                    session_key = '{}'
                    WHERE device_id = '{}'
                '''.format(push_key,
                           is_ios,
                           id_user,
                           session_key,
                           device_id)
                cur.execute(select_up)
                conn.commit()
                conn.close()
                return 1
            except:
                traceback.print_exc()
                conn.close()
                return -1
    return 0


def save_photo_user(id_user,
                    name_photo,
                    cur):
    try:
        select = '''
            INSERT INTO users_photo (id_photo, path_img, created_on, ref_id_user)
            VALUES (DEFAULT , '{}', DEFAULT, {} )
        '''.format(name_photo, id_user)
        cur.execute(select)
        return 1
    except:
        traceback.print_exc()
        return 0
