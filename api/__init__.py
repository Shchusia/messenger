from flask import Flask,render_template,request,redirect,url_for,session,jsonify,blueprints
import traceback
import logging
from logging.handlers import RotatingFileHandler
from api.redis_db.redb import check_valid_data_multi,\
    set_client_multi,\
    remove_user
from api._api_answers import error_valid_data
from celery import Celery
from config import path_celery
app = Flask(__name__)
handler = RotatingFileHandler('foo.log', maxBytes=10000, backupCount=1)
handler.setLevel(logging.INFO)
app.logger.addHandler(handler)
async_mode = None

app.config['CELERY_BROKER_URL'] = path_celery
app.config['CELERY_RESULT_BACKEND'] = path_celery
celery = Celery(app.name, broker=app.config['CELERY_BROKER_URL'])
celery.conf.update(app.config)

app.config['SECRET_KEY'] = 'secret!'
namespace = '/brandager'
'''
server_cred = {
    'id_user': 0,
    'role': 'server',
    'session_key': hashlib.sha256(bytes(str('server') + str(datetime.datetime.now()),'utf-8')).hexdigest()
}

remove_user(server_cred['id_user'],
            server_cred['role'])
set_client_multi(server_cred['id_user'],
                 server_cred['session_key'],
                 server_cred['role'])
server_sid = hashlib.sha256(bytes(str('server') + str(datetime.datetime.now()),'utf-8')).hexdigest()

add_socket(server_cred['session_key'],
           server_cred['id_user'],
           server_sid)
'''


def checker_valid_data_user(fn):
    def wrap(*args, **kwargs):
        try:
            # print(request.headers)
            sk = request.headers['Session-Key']
            iu = request.headers['Id-User']
            r = request.headers['Role']
            # print(iu)
            # print(sk)
            if check_valid_data_multi(iu, sk,r):
                # print('ok')
                return fn(*args, **kwargs)
            else:
                # print('not_valid')
                return jsonify({
                    'code': -1,
                    'message': error_valid_data,
                    'data': {}
                })
        except:
            # print('fucked error')
            traceback.print_exc()
            return jsonify({
                'code': -1,
                'message': error_valid_data,
                'data': {}
            })

    return wrap


@app.route('/')
def test():
    from pprint import pprint
    pprint(str(request.headers))
    return jsonify()


import api.authentification.api_authentification
import api.search.api_search
import api.dialogs.api_dialogs
import api.profile.api_profile
import api.for_celery.pushs
