from api.__init__ import app
from flask import request,\
    jsonify
from api.list_api import api_celery_push_message

from celery_.celery_message import push_message


def loc():
    if request.headers['Host'].find('127.0.0.1') == 0:
        push_message.delay(request.json['id_user'],
                           request.json['message'])
    return jsonify()

app.add_url_rule(api_celery_push_message, 'push', loc, methods=['POST'])