from flask import request, \
    jsonify
import binascii
import os
import traceback
from api.list_api import api_create_dialogs,\
    api_get_message_dialog,\
    api_add_to_important_message,\
    api_get_important_message,\
    api_rem_important,\
    api_create_chat,\
    api_add_user_to_chat,\
    api_upload_file,\
    api_chat_info,\
    api_user_info,\
    api_up_notification_chat
from api.dialogs.db_dialogs import db_create_dialogs,\
    get_message_chat,\
    get_important_message_db,\
    message_to_important_message,\
    delete_from_important_message,\
    create_chat_db, \
    add_to_chat_db,\
    get_information_chat,\
    get_information_user,\
    is_exist_user_in_chat,\
    update_notification
from api._api_answers import error_arguments, \
    error_server,\
    not_user_chat, \
    error_connect,\
    good_select,\
    error_select
from api.config_api import create_connection
from config import str_path_to_file_nginx_gunicorn,\
    str_path_to_file,\
    MEDIA_FOLDER
from api.functions.compres_photo import comppress_photo

from api.__init__ import app,\
    checker_valid_data_user

MAX_FILE_SIZE = 1024 * 1024 + 1


@checker_valid_data_user
def create_dialogs():
    try:
        id_user = request.json['id_user']
        id_us = request.headers['Id-User']
        return jsonify(db_create_dialogs(id_us,
                                         id_user))
    except KeyError:
        return jsonify({
            'code': 0,
            'message': error_arguments,
            'data': {}
        })
    except:
        traceback.print_exc()
        return jsonify({
            'code': 0,
            'message': error_server,
            'data': {}
        })


@checker_valid_data_user
def get_message_dialog(page):
    try:
        id_us = request.headers['Id-User']
        id_chat = request.headers['Id-Chat']
        return jsonify(get_message_chat(id_chat, id_us, page))
    except KeyError:
        return jsonify({
            'code': 0,
            'message': error_arguments,
            'data': {}
        })
    except:
        traceback.print_exc()
        return jsonify({
            'code': 0,
            'message': error_server,
            'data': {}
        })


@checker_valid_data_user
def get_important_message(page):
    try:
        id_us = request.headers['Id-User']
        return jsonify(get_important_message_db(id_us,
                                                page))
    except KeyError:
        return jsonify({
            'code': 0,
            'message': error_arguments,
            'data': {}
        })
    except:
        traceback.print_exc()
        return jsonify({
            'code': 0,
            'message': error_server,
            'data': {}
        })


@checker_valid_data_user
def add_to_important_message():
    try:
        id_us = request.headers['Id-User']
        id_chat = request.json['id_chat']
        id_message = request.json['id_message']
        return jsonify(message_to_important_message(id_us,
                                                    id_chat,
                                                    id_message))
    except KeyError:
        return jsonify({
            'code': 0,
            'message': error_arguments,
            'data': {}
        })
    except:
        traceback.print_exc()
        return jsonify({
            'code': 0,
            'message': error_server,
            'data': {}
        })


@checker_valid_data_user
def rem_important():
    try:
        id_us = request.headers['Id-User']
        id_message = request.json['id_important_message']
        return jsonify(delete_from_important_message(id_us,
                                                     id_message))
    except KeyError:
        return jsonify({
            'code': 0,
            'message': error_arguments,
            'data': {}
        })
    except:
        traceback.print_exc()
        return jsonify({
            'code': 0,
            'message': error_server,
            'data': {}
        })


@checker_valid_data_user
def create_chat():
    try:
        id_us = request.headers['Id-User']
        list_users = request.json['list_user']
        title_chat = request.json['title_chat']
        return jsonify(create_chat_db(list_users,
                                      id_us,
                                      title_chat))
    except KeyError:
        return jsonify({
            'code': 0,
            'message': error_arguments,
            'data': {}
        })
    except:
        traceback.print_exc()
        return jsonify({
            'code': 0,
            'message': error_server,
            'data': {}
        })


@checker_valid_data_user
def add_user_to_chat():
    try:
        id_us = request.headers['Id-User']
        id_user = request.json['id_user']
        id_chat = request.json['id_chat']
        return jsonify(add_to_chat_db(id_us,
                                      id_user,
                                      id_chat))
    except KeyError:
        return jsonify({
            'code': 0,
            'message': error_arguments,
            'data': {}
        })
    except:
        traceback.print_exc()
        return jsonify({
            'code': 0,
            'message': error_server,
            'data': {}
        })


@checker_valid_data_user
def get_information_chat_(id_chat):
    try:
        conn = create_connection()
        if conn:
            cur = conn.cursor()
            id_us = request.headers['Id-User']
            if is_exist_user_in_chat(id_chat, id_us, cur):
                data = get_information_chat(id_chat, cur)
                conn.close()
                return jsonify({
                    'code': 1,
                    'message': good_select,
                    'data': {
                        'chat': data
                    }
                })
            else:
                return jsonify({
                    'code': 1,
                    'message': not_user_chat,
                    'data': {}
                })

        return jsonify({
            'code': 0,
            'message': error_connect,
            'data': {}
        })
    except KeyError:
        return jsonify({
            'code': 0,
            'message': error_arguments,
            'data': {}
        })
    except:
        traceback.print_exc()
        return jsonify({
            'code': 0,
            'message': error_server,
            'data': {}
        })


@checker_valid_data_user
def get_information_user_(id_user):
    try:
        conn = create_connection()
        if conn:
            cur = conn.cursor()
            data = get_information_user(id_user,
                                        cur)
            conn.close()
            return jsonify({
                'code': 1,
                'message': good_select,
                'data': {
                    'user': data
                }
            })

        return jsonify({
            'code': 0,
            'message': error_connect,
            'data': {}
        })
    except KeyError:
        return jsonify({
            'code': 0,
            'message': error_arguments,
            'data': {}
        })
    except:
        traceback.print_exc()
        return jsonify({
            'code': 0,
            'message': error_server,
            'data': {}
        })


# @checker_valid_data_user
def upload_file():
    # print('hello')
    try:
        # print(request.headers)
        # print(request.data)
        file = request.files.get('file')
        is_photo = str(request.headers['is_photo']).lower()

        # file_bytes = file.read()
        if not os.path.exists(MEDIA_FOLDER + str_path_to_file):
            os.makedirs(MEDIA_FOLDER + str_path_to_file)
        name = (str(binascii.hexlify(os.urandom(20)))[2:42]) + '.{}'.format(file.filename.split('.')[-1])
        path = str_path_to_file_nginx_gunicorn + str_path_to_file + name
        out = open(MEDIA_FOLDER + str_path_to_file + name, 'wb')
        while True:
            file_bytes = file.read(MAX_FILE_SIZE)
            out.write(file_bytes)
            if len(file_bytes) < MAX_FILE_SIZE:
                break
        out.close()
        if is_photo == 'true':
            comppress_photo(MEDIA_FOLDER + str_path_to_file + name)
            path = str_path_to_file_nginx_gunicorn + str_path_to_file + '.'.join(name.split('.')[:-1]) + '.jpeg'
        return jsonify({
                'code': 0,
                'message': good_select,
                'data': {
                    'path_to_file': path
                }
        })
    except KeyError:
        return jsonify({
            'code': 0,
            'message': error_arguments,
            'data': {}
        })
    except:
        traceback.print_exc()
        return jsonify({
            'code': 0,
            'message': error_server,
            'data': {}
        })


@checker_valid_data_user
def up_not(id_chat):
    try:
        conn = create_connection()
        if conn:
            id_us = request.headers['Id-User']
            cur = conn.cursor()
            data = update_notification(id_us,
                                       id_chat,
                                       cur)
            if data > 0:
                conn.commit()
                code = 1
                message = good_select
            else:
                code = 0
                message = error_select
            conn.close()
            return jsonify({
                'code': code,
                'message': message,
                'data': {}
            })

        return jsonify({
            'code': 0,
            'message': error_connect,
            'data': {}
        })
    except KeyError:
        return jsonify({
            'code': 0,
            'message': error_arguments,
            'data': {}
        })
    except:
        traceback.print_exc()
        return jsonify({
            'code': 0,
            'message': error_server,
            'data': {}
        })


app.add_url_rule(api_create_dialogs,
                 'create_dialogs',
                 create_dialogs,
                 methods=['POST'])
app.add_url_rule(api_get_message_dialog,
                 'get_message_dialog',
                 get_message_dialog,
                 methods=['GET'])
app.add_url_rule(api_get_important_message,
                 'get_important_message',
                 get_important_message,
                 methods=['GET'])
app.add_url_rule(api_add_to_important_message,
                 'add_to_important_message',
                 add_to_important_message,
                 methods=['POST'])
app.add_url_rule(api_rem_important,
                 'rem_important',
                 rem_important,
                 methods=['POST'])
app.add_url_rule(api_create_chat,
                 'create_chat',
                 create_chat,
                 methods=['POST'])
app.add_url_rule(api_add_user_to_chat,
                 'add_user_to_chat',
                 add_user_to_chat,
                 methods=['POST'])
app.add_url_rule(api_chat_info,
                 'chat_info',
                 get_information_chat_,
                 methods=['GET'])
app.add_url_rule(api_user_info,
                 'user_info',
                 get_information_user_,
                 methods=['GET'])
app.add_url_rule(api_upload_file,
                 'upload_file',
                 upload_file,
                 methods=['POST'])
app.add_url_rule(api_up_notification_chat,
                 'up_not',
                 up_not,
                 methods=['GET'])
