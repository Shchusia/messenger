from api.config_api import create_connection
from api._api_answers import dialog_exist,\
    dialog_not_create, \
    create_dialog,\
    error_connect,\
    good_select,\
    not_user_chat,\
    error_select,\
    not_message_chat,\
    message_exist_in_important,\
    user_exist_in_chat
import traceback
import redis
from socket_server.text_message import user_add_to_chat,\
    create_chat,\
    create_dialog as cd
from socket_server.db_for_socket import get_inf_chat,\
    participant_in_dialogue
from api.redis_db.serv_cache import set_dialog_two_users,\
    get_dialog_two_users
from socket_server.redis_cache import get_name_user as gnu,\
    set_name_user,\
    add_user_to_chat,\
    is_exist_user_in_chat as ieuic

count_message_at_page = 20

r_c = redis.StrictRedis(host='localhost', port=6379, db=14)


def add_contacts(id_user, him_contacts):
    try:
        r_c.lpush(id_user, him_contacts)
    except:
        pass


def find_dialog(id_user1,
                id_user2,
                cur):
    id_dialog = get_dialog_two_users(id_user2,
                                     id_user1)
    if id_dialog is not None:
        return id_dialog
    select = '''
        SELECT id_chat
        FROM chat
        WHERE is_chat = FALSE
        AND id_chat in (SELECT ref_id_chat
                        FROM chat_to_user
                        WHERE ref_id_user = {})
        AND id_chat in (SELECT ref_id_chat
                        FROM chat_to_user
                        WHERE ref_id_user = {})
    '''.format(id_user1, id_user2)
    cur.execute(select)
    try:
        id_chat = cur.fetchone()[0]
        if id_chat:
            set_dialog_two_users(id_user2,
                                 id_user1,
                                 id_chat)
            return int(id_chat)
        return 0
    except:
        return 0


def get_name_user(id_user,
                  cur):
    name = gnu(id_user)
    if name:
        return name
    select = '''
        SELECT full_name
        FROM users
        WHERE id_user = {}
    '''.format(id_user)
    cur.execute(select)
    try:
        name = cur.fetchone()[0]
        set_name_user(id_user,
                      name)
        return name
    except:
        return 'incognito'


def db_create_dialogs(id_user1,
                      id_user2):
    conn = create_connection()
    if conn:
        cur = conn.cursor()
        if int(id_user1) != int(id_user2):
            id_dialog = find_dialog(id_user1, id_user2, cur)
        else:
            id_dialog = 1
        if id_dialog == 0:
            titl = get_name_user(id_user2, cur)
            try:
                select = '''
                    INSERT INTO chat (id_chat, title_chat, created_on, is_chat)
                    VALUES (DEFAULT, '{}', DEFAULT , FALSE) Returning id_chat
                '''.format(titl)
                cur.execute(select)
                id_di = cur.fetchone()[0]
                if int(id_user1) != int(id_user2):
                    select = '''
                        INSERT INTO chat_to_user (ref_id_chat, ref_id_user)
                        VALUES ({}, {});
                        INSERT INTO chat_to_user (ref_id_chat, ref_id_user)
                        VALUES ({}, {});
                    '''.format(id_di, id_user1,
                               id_di, id_user2)
                    add_contacts(id_user1, id_user2)
                    add_contacts(id_user2, id_user1)
                    add_user_to_chat(id_di,id_user1)
                    add_user_to_chat(id_di,id_user2)

                else:
                    select = '''

                                            INSERT INTO chat_to_user (ref_id_chat, ref_id_user)
                                            VALUES ({}, {});
                                        '''.format(id_di, id_user1)
                cur.execute(select)
                select = '''
                    INSERT INTO chat_message (id_chat_message, message, ref_id_chat, ref_id_user, created_on, type_mess)
                    VALUES (DEFAULT, '{}', {},{},DEFAULT , 8)
                '''.format(cd.format(get_name_user(id_user1,cur)), id_di, id_user1)
                cur.execute(select)
                data = {
                        'chat': get_information_chat(id_di, cur)
                    }
                conn.commit()
                return {
                    'code': 1,
                    'message': create_dialog,
                    'data': data
                }
            except:
                traceback.print_exc()
                return {
                    'code': 1,
                    'message': dialog_not_create,
                    'data': {}
                }
        return {
            'code': 1,
            'message': dialog_exist,
            'data': {}
        }
    return {
            'code': 0,
            'message': error_connect,
            'data': {}
        }


def is_exist_user_in_chat(id_chat,
                          id_user,
                          cur):
    res = ieuic(id_chat, id_user)
    if res:
        return True
    select = '''
        SELECT get_notification
        FRom chat_to_user
        WHERE ref_id_user = {}
        AND ref_id_chat = {}
    '''.format(id_user,id_chat)
    cur.execute(select)
    try:
        row = cur.fetchone()
        if row[0] is not None:
            add_user_to_chat(id_chat, id_user)
            return True
        else:
            return False
    except:
        return False


def get_name_user_(id_user,
                   cur):
    name = gnu(id_user)
    if name:
        return name
    select = '''
        SELECT full_name
        FROM users
        WHERE id_user = {}
    '''.format(id_user)
    cur.execute(select)
    name = cur.fetchone()[0]
    set_name_user(id_user,
                  name)
    return name


# def get_inf_chat(id_chat,
#                  cur):
#     select = '''
#         SELECT is_chat, title_chat
#         FROM chat
#         WHERE id_chat = {}
#     '''.format(id_chat)
#     cur.execute(select)
#     row = cur.fetchone()
#     return row[0], row[1]


def get_message_chat(id_chat,
                     id_user,
                     page=0):
    conn = create_connection()
    if conn:
        cur = conn.cursor()
        if is_exist_user_in_chat(id_chat,id_user,cur):
            try:
                is_chat, title_chat = get_inf_chat(id_chat, cur)

                select = '''
                    SELECT cm.message, cm.ref_id_user, cm.created_on, cm.id_chat_message,cm.type_mess
                    FROM chat_message cm
                    WHERE cm.ref_id_chat = {}
                    ORDER BY cm.created_on DESC limit {} offset {}
                '''.format(id_chat,count_message_at_page, count_message_at_page * int(page))
                cur.execute(select)
                names = {}
                message = []
                rows = cur.fetchall()
                for row in rows:
                    if names.get(row[1], False):
                        name = names[row[1]]
                    else:
                        name = get_name_user(row[1], cur)
                        names[row[1]] = name
                    message.append({
                        'text_message': row[0],
                        'name': name,
                        'id_chat': int(id_chat),
                        'id_sender': int(row[1]),
                        'date_message': str(row[2]),
                        'is_chat': is_chat,
                        'title_chat': title_chat,
                        'id_message': int(row[3]),
                        'type_message': int(row[4])
                    })
                return {
                    'code': 1,
                    'message': good_select,
                    'data': {
                        'messages': message
                    }
                }
            except:
                return {
                    'code': 0,
                    'message': error_select,
                    'data': {}
                }
        else:
            #  юзер не имеет отношения к диалогу
            return {
                'code': 1,
                'message': not_user_chat,
                'data': {}
            }
    return {
        'code': 0,
        'message': error_connect,
        'data': {}
    }


def is_exist_message_in_chat(id_chat, id_message, cur):
    select = '''
            SELECT id_chat_message
            FRom chat_message
            WHERE id_chat_message = {}
            AND ref_id_chat = {}
        '''.format(id_message, id_chat)
    cur.execute(select)
    try:
        row = cur.fetchone()
        if row[0] is not None:
            return True
        else:
            return False
    except:
        return False


def message_to_important_message(id_user,
                                 id_chat,
                                 id_message):
    conn = create_connection()
    if conn:
        try:
            cur = conn.cursor()
            if is_exist_user_in_chat(id_chat, id_user, cur):
                if is_exist_message_in_chat(id_chat,id_message, cur):
                    select = '''
                         SELECT id_chat_message,message,ref_id_user,ref_id_chat,created_on
                         FRom chat_message
                         WHERE id_chat_message = {}
                         AND ref_id_chat = {}
                    '''.format(id_message, id_chat)
                    cur.execute(select)
                    row = cur.fetchone()
                    try:
                        ins = '''
                            INSERT INTO chat_important_message (id_chat_important_message, message, ref_id_chat, ref_id_user, created_on, ref_id_chat_message, ref_id_user_for_who_imp)
                            VALUES (DEFAULT, '{}', {}, {},'{}', {}, {})
                        '''.format(row[1], id_chat, row[2], row[4], row[0], id_user)
                        cur.execute(ins)
                        conn.commit()
                        message = good_select
                    except:
                        message = message_exist_in_important
                    conn.close()
                    return {
                        'code': 1,
                        'message': message,
                        'data': {}
                }

            else:
                #  сообщение не относится к чату
                return {
                    'code': 1,
                    'message': not_message_chat,
                    'data': {}
                }

        except:
            return {
                'code': 0,
                'message': error_select,
                'data': {}
            }
    return {
        'code': 0,
        'message': error_connect,
        'data': {}
    }


def delete_from_important_message(id_message_imp,
                                  id_user):
    conn = create_connection()
    if conn:
        try:
            cur = conn.cursor()
            select = '''
                DELETE FROM chat_important_message WHERE id_chat_important_message = {} AND ref_id_user_for_who_imp = {} RETURNING id_chat_important_message
            '''.format(id_message_imp, id_user, id_user)
            cur.execute(select)
            rows = cur.fetchall()
            if rows:
                return {
                    'code': 0,
                    'message': good_select,
                    'data': {}
            }
            return {
                    'code': 0,
                    'message': not_message_chat,
                    'data': {}
            }
        except:
            return {
                'code': 0,
                'message': error_select,
                'data': {}
            }
    return {
        'code': 0,
        'message': error_connect,
        'data': {}
    }


def get_important_message_db(id_user,
                             page=0):
    conn = create_connection()
    if conn:
        try:
            cur = conn.cursor()
            select = '''
                SELECT cim.message, u.full_name, cim.ref_id_chat,cim.ref_id_user,cim.created_on,c.is_chat,c.title_chat,cim.ref_id_chat_message, cim.id_chat_important_message
                FROM chat_important_message cim, chat c, users u
                WHERE cim.ref_id_user_for_who_imp = {}
                AND cim.ref_id_user = u.id_user
                AND c.id_chat = cim.ref_id_chat
                ORDER BY cim.id_chat_important_message DESC LIMIT {} offset {}
            '''.format(id_user,
                       count_message_at_page,
                       count_message_at_page * int(page))

            cur.execute(select)
            rows = cur.fetchall()
            ans = []
            for r in rows:
                ans.append({
                    'text_message': r[0],
                    'name': r[1],
                    'id_chat': r[2],
                    'id_sender': r[3],
                    'date_message': str(r[4]),
                    'is_chat': r[5],
                    'title_chat': r[6],
                    'id_message': r[7],
                    'is_deleted': False,
                    'id_in_important': r[8]
                })
            return {
                'code': 0,
                'message': good_select,
                'data': {
                    'important_messages': ans
                }
            }
        except:
            return {
                'code': 0,
                'message': error_select,
                'data': {}
            }
    return {
        'code': 0,
        'message': error_connect,
        'data': {}
    }


def create_chat_db(list_user,
                   id_user,
                   title_chat):
    conn = create_connection()
    if conn:
        cur = conn.cursor()
        select = '''
            INSERT INTO chat (id_chat, title_chat, created_on, is_chat)
            VALUES (DEFAULT, '{}', DEFAULT , TRUE ) Returning id_chat
        '''.format(title_chat)
        try:
            cur.execute(select)
            id_chat = cur.fetchone()[0]
            select = '''
                INSERT INTO chat_to_user (ref_id_chat, ref_id_user)
                VALUES ({}, {})
            '''
            list_user.append(id_user)
            lu = list(set(list_user))
            for l in lu:
                try:
                    cur.execute(select.format(id_chat, l))
                    conn.commit()
                    add_user_to_chat(id_chat,
                                     l)
                except:
                    conn.close()
                    conn = create_connection()
                    cur = conn.cursor()
            select_inser = '''
                INSERT INTO chat_message (id_chat_message, message, ref_id_chat, ref_id_user, created_on, type_mess)
                VALUES (DEFAULT , '{}', {},{},DEFAULT , 7)
            '''.format(create_chat.format(get_name_user(id_user,
                                                        cur),
                                          title_chat),
                       id_chat,
                       id_user)
            cur.execute(select_inser)
            conn.commit()
            # sio.emit('add_to_chat', {'id_chat': id_chat}, namespace=namespace)
            data = {
                'code': 1,
                'message': good_select,
                'data': {
                    'chat': get_information_chat(id_chat,
                                                 cur)
                }
            }
            conn.close()
            return data
        except:
            traceback.print_exc()
            return {
                'code': 0,
                'message': error_select,
                'data': {}
            }
    return {
        'code': 0,
        'message': error_connect,
        'data': {}
    }


def add_to_chat_db(id_user,
                   id_user_new,
                   id_chat):
    select = '''
        SELECT ref_id_user
        FROM chat_to_user
        WHERE ref_id_chat = {}
    '''.format(id_chat)
    conn = create_connection()
    if conn:
        cur = conn.cursor()
        cur.execute(select)
        rows = {r[0]: r[0] for r in cur.fetchall()}
        if rows.get(int(id_user), False):
            if rows.get(int(id_user_new), True):
                # добавить
                insert = '''
                    INSERT INTO chat_to_user (ref_id_chat, ref_id_user)
                    VALUES ({}, {})
                '''.format(id_chat, id_user_new)
                cur.execute(insert)
                conn.commit()
                # add_user_to_chat(id_chat, id_user_new)
                mess = user_add_to_chat.format(get_name_user(id_user,cur), get_name_user(id_user_new, cur))
                ins_mes = '''
                    INSERT INTO chat_message (id_chat_message, message, ref_id_chat, ref_id_user, created_on, type_mess)
                    VALUES (DEFAULT, '{}', {}, {},DEFAULT , 5)
                '''.format(mess, id_chat, id_user_new)
                cur.execute(ins_mes)
                conn.commit()
                id_chat, title_chat = get_inf_chat(id_chat,cur)
                conn.close()
                add_user_to_chat(id_chat,
                                 id_user_new)
                #sio.emit('add_to_chat_new_user', {
                #    'id_chat': id_chat,
                #    'id_user_new': id_user_new,
                #    'id_user_who': id_user},
                #         namespace=namespace)
                return {
                    'code': 1,
                    'message': good_select,
                    'data': {
                        'id_chat': id_chat,
                        'id_user_new': id_user_new,
                        'id_user_who': id_user,
                        'title_chat': title_chat}
                }
            else:
                return {
                    'code': 1,
                    'message': user_exist_in_chat,
                    'data': {}
                }
        else:
            return {
                'code': 1,
                'message': not_user_chat,
                'data': {}
            }
    return {
        'code': 0,
        'message': error_connect,
        'data': {}
    }


def get_information_user(id_user, cur):
    select = '''
        SELECT u.id_user, u.full_name, u.login
        FROM users u
        WHERE u.id_user = {}
    '''.format(id_user)
    cur.execute(select)
    row = cur.fetchone()
    return {
        'id_user': row[0],
        'role': 'user',
        'full_name': row[1],
        'login': row[2]
    }


def get_information_chat(id_chat, cur):
    select_chat = '''
        SELECT c.id_chat, c.title_chat,c.is_chat, c.created_on
        FROM chat c
        WHERE c.id_chat = {}
    '''.format(id_chat)

    select_us = '''
        SELECT cu.ref_id_user
        FROM chat_to_user cu
        WHERE cu.ref_id_chat = {}
    '''.format(id_chat)

    cur.execute(select_chat)
    chat = cur.fetchone()
    cur.execute(select_us)
    us = cur.fetchall()
    users = []
    for u in us:
        users.append(get_information_user(u[0], cur))
    data = {
        'users': users,
        'is_chat': chat[2],
        'id_chat': chat[0],
        'title_chat': chat[1],
        'date_create': chat[3]
    }
    return data


def send_message_db(user_id,
                    message,
                    id_chat,
                    type_me=3):
    conn = create_connection()
    if conn:
        cur = conn.cursor()
        is_ex = is_exist_user_in_chat(id_chat, user_id, cur)
        if is_ex:  # проверка отношения пользователя к чату
            insert_message = '''
                INSERT INTO chat_message (id_chat_message, message, ref_id_chat, ref_id_user, created_on, type_mess)
                VALUES (DEFAULT, '{}', {},{}, DEFAULT) Returning created_on, id_chat_message
            '''.format(message.replace("'", "\'"), id_chat, user_id)
            try:
                cur.execute(insert_message)
                conn.commit()
                row = cur.fetchone()
                date = row[0]
                id_mess = row[1]
                parti_dia = participant_in_dialogue(id_chat, user_id, cur)
                for p in parti_dia:
                    select = '''
                        INSERT INTO chat_new_message (id_new_message, ref_id_user, ref_id_chat)
                        VALUES (DEFAULT , {}, {} )
                    '''.format(p, id_chat)
                    cur.execute(select)
                    conn.commit()
                name = get_name_user(user_id, cur)
                is_chat, title_chat = get_inf_chat(id_chat, cur)
                conn.close()
                return parti_dia, 1, {
                    'text_message': message,
                    'name': name,
                    'id_chat': id_chat,
                    'id_sender': user_id,
                    'date_message': str(date),
                    'is_chat': is_chat,
                    'title_chat': title_chat,
                    'is_deleted': False,
                    'id_message': id_mess
                }
            except:
                traceback.print_exc()
                return [], -1, 0  # 'сообщение не добавлено'
            pass
        else:
            return [], -2, 0  # 'не имеешь отношения к чату'


def update_notification(id_chat, id_user,cur):
    try:
        select = '''
        UPDATE chat_to_user
        set get_notification = NOT get_notification
        WHERE ref_id_user = {}
        AND ref_id_chat = {}
        '''.format(id_user, id_chat)
        cur.execute(select)
        return 1
    except:
        return -1


def entry_to_account(id_user, message):
    def get_id_chat(cur):
        # conn = create_connection()
        # cur = conn.cursor()
        select = '''
            select DISTINCT c.ref_id_chat
            FROM chat_to_user  c, chat ch
            WHERE c.ref_id_chat in (SELECT ctu.ref_id_chat FROM chat_to_user ctu WHERE ctu.ref_id_user = {})
            and  c.ref_id_chat in (SELECT ct.ref_id_chat FROM chat_to_user ct WHERE ct.ref_id_user = {})
            and c.ref_id_chat = ch.id_chat
            and ch.is_chat = FALSE
        '''.format(id_user, 1)
        cur.execute(select)
        id_chat = cur.fetchone()
        # conn.close()
        return id_chat

    conn = create_connection()
    if conn:
        cur = conn.cursor()
        id_chat = get_id_chat(cur)
        conn.close()
        return send_message_db(id_user,
                               message,
                               id_chat)






