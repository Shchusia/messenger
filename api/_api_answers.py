good_sign_in = {
    'code': 1,
    'title': 'ok',
    'body': 'вы удачно вошли'
}

good_logout = {
    'code': 2,
    'title': 'оk',
    'body': 'успешно вышли'
}

good_select = {
    'code': 3,
    'title': 'оk',
    'body': 'успешный запрос'
}

create_dialog ={
    'code': 4,
    'title': 'оk',
    'body': 'диалог создан'
}

user_with_login_exist = {
    'code': -1,
    'title': 'ошибка',
    'body': 'пользователь с таким login существует'
}

not_valid_data_entry = {
    'code': -2,
    'title': 'ошибка',
    'body': 'неверный логин или(и) пароль'
}

dialog_exist = {
    'code': -3,
    'title': 'ошибка',
    'body': 'диалог существует'
}

dialog_not_create = {
    'code': -4,
    'title': 'ошибка',
    'body': 'диалог просто не создан'
}

message_not_send = {
    'code': -6,
    'title': 'ошибка',
    'body': 'сообщение не добавлено'
}

not_user_chat = {
    'code': -7,
    'title': 'ошибка',
    'body': 'вы не имеете отношения к данному чату'
}

# -5 -6 -7 -8 -9 for

not_message_chat = {
    'code': -12,
    'title': 'ошибка',
    'body': 'вы не имеете отношения к данному сообщению'
}

message_exist_in_important = {
    'code': -13,
    'title': 'ошибка',
    'body': 'сообщение уже в избранных'
}

user_exist_in_chat = {
    'code': -14,
    'title': 'ошибка',
    'body': 'пользователь существует в чате'
}

error_connect = {
    'code': -30,
    'title': 'ошибка',
    'body': 'ошибка подключения к бд'
}

error_select = {
    'code': -31,
    'title': 'ошибка',
    'body': 'ошибка запроса к бд'
}

error_arguments = {
    'code': -32,
    'title': 'ошибка',
    'body': 'нет данных необходимых для выполнения запроса'
}

error_server = {
    'code': -33,
    'title': 'ошибка',
    'body': 'неизвестная ошибка'
}

error_valid_data = {
    'code': -34,
    'title': 'оk',
    'body': 'невалидные данные '
}
