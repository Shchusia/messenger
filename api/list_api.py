domen = '/my_message/v1/api'

api_registration = domen + '/registration'
api_log_in = domen + '/log_in'
api_log_out = domen + '/log_out'
api_check_valid_data_user = domen + '/check_valid_data'


api_find_in_all_user = domen + '/find/user/<page>'
api_find_in_contacts = domen + '/find/contacts/<page>'

api_get_contacts = domen + '/contacts/<page>'
api_get_dialogs = domen + '/dialogs/<page>'

api_create_dialogs = domen + '/create/dialog'
api_upload_file = domen + '/send/file/message'
api_get_message_dialog = domen + '/messages/chat/<page>'
api_chat_info = domen + '/chat/info/<id_chat>'
api_user_info = domen + '/user/info/<id_user>'

api_get_important_message = domen + '/important/message/<page>'
api_add_to_important_message = domen + '/to_important'
api_rem_important = domen + '/remove/important'

api_create_chat = domen + '/create/chat'
api_add_user_to_chat = domen + '/add/to/chat'
api_leave_chat = domen + '/leave_chat/<id_chat>'

api_update_profile = domen + '/profile/up'
api_profile = domen + '/profile'

api_celery_push_message = domen + '/push_message'

api_up_notification_chat = domen + '/up_notification/<id_chat>'
