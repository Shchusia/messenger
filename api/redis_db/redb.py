import redis
import traceback

ttl_phone_in_redis = 300

r = redis.StrictRedis(host='localhost', port=6379, db=2)


def set_user_multi(id_user, session_key):
    r.lpush('user_{}'.format(id_user), session_key)
    # r.expire(session_key, 60 * 60 * 24 * 31)  # если надо ттл


def set_client_multi(id_user,session_key, role):
    r.lpush('{}_{}'.format(role,id_user), session_key)


def set_user_not_multi(id_user, session_key):
    r.set('user_{}'.format(id_user), session_key)
    # r.expire(session_key, 60 * 60 * 24 * 31)  # если надо ттл


def set_boss_multi(id_user, session_key):
    r.lpush('boss_{}'.format(id_user), session_key)
    # r.expire(session_key, 60 * 60 * 24 * 31)  # если надо ттл


def set_boss_not_multi(id_user, session_key):
    r.set('boss_{}'.format(id_user), session_key)
    # r.expire(session_key, 60 * 60 * 24 * 31)  # если надо ттл


def check_valid_data_multi(id_user, session_key, role):
    try:
        l = list(r.lrange('{}_{}'.format(role,id_user), 0, -1))
        # print(len(l))
        # print('___')
        for li in l:
            # print(li)
            if li.decode('utf-8') == session_key:
                return True
        return False
    except:
        traceback.print_exc()
    return False


def check_valid_data_not_multi(id_user, session_key, role):
    try:
        sk = r.get('{}_{}'.format(role, id_user))
        if sk.decode('utf-8') == session_key:
            return True
        return False
    except:
        traceback.print_exc()
    return False


def logout_not_multi(id_user, session_key, role):
    try:
        r.delete('{}_{}'.format(role, id_user))
    except:
        pass


def logout_multi(id_user, session_key, role):
    try:
        r.lrem('{}_{}'.format(role, id_user), 1, session_key)
    except:
        pass


def remove_user(id_user, role):
    # print('dasdasda')
    try:
        l = list(r.lrange('{}_{}'.format(role, id_user), 0, -1))
        for li in l:
            try:
                r.delete(li)
            except:
                traceback.print_exc()
                pass
        r.delete('{}_{}'.format(role, id_user))
    except:
        traceback.print_exc()