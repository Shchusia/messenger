import redis


r = redis.StrictRedis(host='localhost', port=6379, db=11)


def set_dialog_two_users(id_user1,
                         id_user2,
                         id_dialog):
    r.set('{}_{}'.format(id_user1,
                         id_user2),
          id_dialog)


def get_dialog_two_users(id_user1,
                         id_user2):
    res = r.get('{}_{}'.format(id_user2,
                               id_user1))
    if res:
        return int(res.decode('utf-8'))
    res = r.get('{}_{}'.format(id_user2,
                               id_user1))
    if res:
        return int(res.decode('utf-8'))
    return None


#print(get_dialog_two_users(1,2))

