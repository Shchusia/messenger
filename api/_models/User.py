import datetime
import hashlib
import traceback
from api.config_api import create_connection
from config import str_path_to_file_nginx_gunicorn,\
    str_path_to_photo_profile


class User:
    password_hash = None
    full_name = None
    login = None
    id = None
    session_key = None
    role = None

    def __init__(self,
                 login=None,
                 password_hash=None,
                 full_name=None,
                 mail=None,
                 id_user=None):
        if login:
            self.login = login
        if password_hash:
            self.password_hash = password_hash
        if full_name:
            self.full_name = full_name
        if mail:
            self.mail = mail
        if id_user:
            self.id = id_user

    @staticmethod
    def create_session_key(pass_word):
        return hashlib.sha256(
            bytes(str(pass_word)
                  + str(datetime.datetime.now()),
                  'utf-8')) \
            .hexdigest()

    def check_user(self,
                   cur,
                   login=None,
                   password=None):

        select = ''
        if login and password:
            self.password_hash = password
            self.login = login
            select = '''
                SELECT  check_user('{}', '{}')
            '''.format(login,
                       password)
        elif self.login and self.password_hash:
            select = '''
                SELECT check_user('{}', '{}')
            '''.format(self.login,
                       self.password_hash)
        else:
            assert AttributeError
        # print(select)
        cur.execute(select)
        res = cur.fetchone()
        if res[0] > 0:
            self.id = int(res[0])
            self.role = 'user'
            self.session_key = self.create_session_key(self.password_hash)
            code = 1
        else:
            code = -1
        return code

    def registration(self,
                     cur,
                     login=None,
                     password=None,
                     full_name=None):
        def create_session_key(pass_word):
            return hashlib.sha256(
                bytes(str(pass_word)
                      + str(datetime.datetime.now()),
                      'utf-8')) \
                .hexdigest()

        select = ''
        # print(self.full_name + '   ' + self.password_hash + '   ' + self.login)
        if login and password:
            self.password_hash = password
            self.login = login
            self.full_name = full_name
            select = '''
                INSERT INTO users (full_name, login, password_hash)
                VALUES ('{}','{}', '{}') RETURNING id_user
            '''.format(full_name,
                       login,
                       password)
        elif self.login and self.password_hash and self.full_name is not None:
            select = '''
              INSERT INTO users (full_name,  login, password_hash)
              VALUES ('{}','{}', '{}') RETURNING  id_user
            '''.format(self.full_name,
                       self.login,
                       self.password_hash)
        else:
            assert AttributeError
        try:
            cur.execute(select)
            res = cur.fetchone()
            self.id = int(res[0])
            select = '''
                INSERT INTO chat (id_chat, title_chat, created_on, is_chat)
                VALUES (DEFAULT, 'Pадость интроверта', DEFAULT, FALSE) RETURNING id_chat
            '''
            cur.execute(select)
            res = cur.fetchone()
            id_c = int(res[0])
            select = '''
                INSERT INTO chat_message (id_chat_message, message, ref_id_chat, ref_id_user, type_mess, created_on)  
                VALUES (DEFAULT, '{}', {}, {},1, DEFAULT )
            '''.format('Здесь Вы можете общаться с собой',
                       id_c,
                       self.id)
            cur.execute(select)
            select = '''
                INSERT INTO chat_to_user (ref_id_chat, ref_id_user)
                VALUES ({}, {})
            '''.format(id_c,
                       self.id)
            cur.execute(select)
            select = '''
                            INSERT INTO chat (id_chat, title_chat, created_on, is_chat)
                            VALUES (DEFAULT, 'Администрация', DEFAULT, FALSE) RETURNING id_chat
                        '''
            cur.execute(select)
            res = cur.fetchone()
            id_c = int(res[0])
            select = '''
                            INSERT INTO chat_message (id_chat_message, message, ref_id_chat, ref_id_user, type_mess, created_on)  
                            VALUES (DEFAULT, '{}', {}, 1, 1, DEFAULT )
                        '''.format('Добро пожаловать!',
                                   id_c,
                                   1)
            cur.execute(select)
            select = '''
                            INSERT INTO chat_to_user (ref_id_chat, ref_id_user)
                            VALUES ({}, {})
                        '''.format(id_c, self.id)
            cur.execute(select)
            select = '''
                                        INSERT INTO chat_to_user (ref_id_chat, ref_id_user)
                                        VALUES ({}, {})
                                    '''.format(id_c, 1)
            cur.execute(select)
            self.role = 'user'
            self.session_key = create_session_key(self.password_hash)
            code = 1
        except:
            traceback.print_exc()
            code = -1
        return code

    def get_user(self):
        if self.full_name and self.id and self.login and False:
            return {
                'id_user': self.id,
                'role': 'user',
                'full_name': self.full_name,
                'login': self.login,
                'details': {}
            }
        else:
            conn = create_connection()
            try:
                select = '''
                    SELECT id_user, full_name, login,is_male
                    FROM users
                    WHERE id_user = {}
                '''.format(self.id)
                cur = conn.cursor()
                cur.execute(select)
                row = cur.fetchone()
                sel_ph = '''
                            select id_photo, path_img, created_on
                            from users_photo
                            WHERE ref_id_user = {}
                            ORDER BY created_on DESC
                        '''.format(self.id)
                cur.execute(sel_ph)
                pho = cur.fetchone()
                if pho:
                    photo = [{
                                 'id_photo': pho[0],
                                 'path_img': str_path_to_file_nginx_gunicorn + str_path_to_photo_profile + pho[1],
                                 'created_on': str(pho[2])
                             }]
                else:
                    if row[3] is None:
                        photo = [{
                            'id_photo': -1,
                            'path_img': str_path_to_file_nginx_gunicorn
                                        + str_path_to_photo_profile
                                        + '_default-inc.png',
                            'created_on': '1970-01-01 00:00:00.000000'
                        }]
                    elif row[3]:
                        photo = [{
                            'id_photo': -2,
                            'path_img': str_path_to_file_nginx_gunicorn
                                        + str_path_to_photo_profile
                                        + '_default-m.png',
                            'created_on': '1970-01-01 00:00:00.000000'
                        }]
                    else:
                        photo = [{
                            'id_photo': -3,
                            'path_img': str_path_to_file_nginx_gunicorn
                                        + str_path_to_photo_profile
                                        + '_default-f.png',
                            'created_on': '1970-01-01 00:00:00.000000'
                        }]

                cur.close()
                conn.close()
                return {
                    'id_user': row[0],
                    'role': 'user',
                    'full_name': row[1],
                    'login': row[2],
                    'details': {},
                    'actual_photo': photo[0]
                }
            except:
                traceback.print_exc()
                pass

    def get_profile(self,
                    id_user,
                    cur=None):
        select = '''
            SELECT id_user, full_name, login, phone,email, about_me, birthday,is_male
            FROM users
            WHERE id_user = {}

        '''.format(id_user)
        if cur is None:
            conn = create_connection()
            cur = conn.cursor()
        cur.execute(select)
        row = cur.fetchone()
        sel_ph = '''
            select id_photo, path_img, created_on
            from users_photo
            WHERE ref_id_user = {}
            ORDER BY created_on DESC
        '''.format(id_user)
        cur.execute(sel_ph)
        pho = cur.fetchall()
        try:
            conn.close()
        except:
            pass
        self.id = id_user
        photo = {}
        if pho:
            photo = [{
                'id_photo': ph[0],
                'path_img': str_path_to_file_nginx_gunicorn
                            + str_path_to_photo_profile
                            + ph[1],
                'created_on': str(ph[2])
            } for ph in pho]
        else:
            if row[7] is None:
                photo = [{
                    'id_photo': -1,
                    'path_img': str_path_to_file_nginx_gunicorn
                                + str_path_to_photo_profile
                                + '_default-inc.png',
                    'created_on': '1970-01-01 00:00:00.000000'
                }]
            elif row[7]:
                photo = [{
                    'id_photo': -2,
                    'path_img': str_path_to_file_nginx_gunicorn
                                + str_path_to_photo_profile
                                + '_default-m.png',
                    'created_on': '1970-01-01 00:00:00.000000'
                }]
            else:
                photo = [{
                    'id_photo': -3,
                    'path_img': str_path_to_file_nginx_gunicorn
                                + str_path_to_photo_profile
                                + '_default-f.png',
                    'created_on': '1970-01-01 00:00:00.000000'
                }]

        return {
            'id_user': row[0],
            'full_name': row[1],
            'role': 'user',
            'login': row[2],
            'actual_photo': photo[0],
            'details': {
                'phone': row[3],
                'email': row[4],
                'about_me': row[5],
                'birthday': row[6],
                'is_male': row[7],
                'photos': photo
            }
        }

