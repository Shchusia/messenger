from flask import request, \
    jsonify
import traceback
from api.list_api import api_find_in_all_user,\
    api_find_in_contacts,\
    api_get_contacts,\
    api_get_dialogs
from api._api_answers import error_arguments,\
    error_server,\
    good_select
from api.search.db_search import find_user_in_contacts,\
    find_user,\
    get_contacts,\
    get_dialogs

from api.__init__ import app,\
    checker_valid_data_user


@checker_valid_data_user
def find_in_all_user(page):
    try:
        name = request.json['name']
        id_user = request.headers['Id-User']
        res = find_user(name, id_user, int(page))
        return jsonify({
            'code': 1,
            'message': good_select,
            'data': {
                'users': res
            }
        })
    except KeyError:
        return jsonify({
            'code': 0,
            'message': error_arguments,
            'data': {}
        })
    except:
        traceback.print_exc()
        return jsonify({
                            'code': 0,
                            'message': error_server,
                            'data': {}
                        })


@checker_valid_data_user
def find_in_contacts(page):
    try:
        name = request.json['name']
        id_user = request.headers['Id-User']
        res = find_user_in_contacts(name,
                                    id_user,
                                    page)
        return jsonify({
            'code': 1,
            'message': good_select,
            'data': {
                'users': res
            }
        })
    except KeyError:
        return jsonify({
            'code': 0,
            'message': error_arguments,
            'data': {}
        })
    except:
        traceback.print_exc()
        return jsonify({
            'code': 0,
            'message': error_server,
            'data': {}
        })


@checker_valid_data_user
def get_contactss(page):
    try:
        id_user = request.headers['Id-User']
        res = get_contacts(id_user, int(page))
        return jsonify({
            'code': 1,
            'message': good_select,
            'data': {
                'users': res
            }
        })
    except KeyError:
        return jsonify({
            'code': 0,
            'message': error_arguments,
            'data': {}
        })
    except:
        traceback.print_exc()
        return jsonify({
                            'code': 0,
                            'message': error_server,
                            'data': {}
                        })


@checker_valid_data_user
def get_dialogss(page):
    try:
        id_user = request.headers['Id-User']
        res = get_dialogs(id_user, int(page))
        return jsonify({
            'code': 1,
            'message': good_select,
            'data': {
                'chats': res
            }
        })
    except KeyError:
        return jsonify({
            'code': 0,
            'message': error_arguments,
            'data': {}
        })
    except:
        traceback.print_exc()
        return jsonify({
            'code': 0,
            'message': error_server,
            'data': {}
        })


app.add_url_rule(api_find_in_all_user,
                 'find_in_all_user',
                 find_in_all_user,
                 methods=['POST'])
app.add_url_rule(api_find_in_contacts,
                 'find_in_contacts',
                 find_in_contacts,
                 methods=['POST'])
app.add_url_rule(api_get_contacts,
                 'get_contacts',
                 get_contactss)
app.add_url_rule(api_get_dialogs,
                 'get_dialogss',
                 get_dialogss)
