from api.config_api import create_connection
import traceback
from datetime import datetime
from api._models.User import User
from config import str_path_to_file_nginx_gunicorn,str_path_to_photo_profile
from socket_server.redis_cache import get_events_user_chat,\
    set_event_user_from_chat
from api.dialogs.db_dialogs import find_dialog
from socket_server.redis_cache import get_name_user as gnu,\
    set_name_user,\
    get_users_chat as guc,\
    add_users_to_chat

user_on_page = 8


# def find_dialog(id_user1,
#                 id_user2,
#                 cur):
#     select = '''
#         SELECT id_chat
#         FROM chat
#         WHERE is_chat = FALSE
#         AND id_chat in (SELECT ref_id_chat
#                         FROM chat_to_user
#                         WHERE ref_id_user = {})
#         AND id_chat in (SELECT ref_id_chat
#                         FROM chat_to_user
#                         WHERE ref_id_user = {})
#     '''.format(id_user1, id_user2)
#     #print(select)
#     try:
#         cur.execute(select)
#
#         id_chat = cur.fetchone()
#         if id_chat:
#             return int(id_chat[0])
#         return 0
#     except:
#
#         traceback.print_exc()
#         return 0


def get_name_user_by_id(id_user, cur):
    name = gnu(id_user)
    if name:
        return name
    select = '''
            SELECT full_name
            FROM users
            WHERE id_user = {}
        '''.format(id_user)
    cur.execute(select)
    try:
        name = cur.fetchone()[0]
        set_name_user(id_user,
                      name)
        return name
    except:
        return 'incognito'


def get_last_message_chat(id_chat,
                          cur):
    select = '''
        SELECT message, ref_id_user, chat_message.created_on, is_chat, title_chat, id_chat_message, type_mess
        FROM chat_message, chat
        WHERE id_chat_message = (
                                  SELECT max(id_chat_message)
                                  FROM chat_message
                                  WHERE ref_id_chat = {})
        AND id_chat = ref_id_chat


    '''.format(id_chat)
    try:
        cur.execute(select)
        row = cur.fetchone()
        return {
            'text_message': row[0],
            'id_sender': row[1],
            'date_message': str(row[2]),
            'id_chat': id_chat,
            'name': get_name_user_by_id(row[1], cur),
            'is_chat': row[3],
            'title_chat': row[4],
            'id_message': row[5],
            'is_deleted': row[6],
            'type_message': row[6],
        }


    except:
        # traceback.print_exc()
        return {}


def find_user(name,
              id_user,
              page=0):
    select = '''
        SELECT DISTINCT (f.id_user),u.full_name, u.login, f.similarit
        FROM find('{}')f, users u
        WHERE u.id_user = f.id_user
        AND u.id_user != {}
        ORDER BY similarit DESC, id_user DESC
        LIMIT {} OFFSET {}
    '''.format(name,
               id_user,
               user_on_page,
               user_on_page * int(page))
    #print(select)
    conn = create_connection()
    if conn:
        try:
            cur = conn.cursor()
            cur.execute(select)
            res = []
            rows = cur.fetchall()

            for r in rows:
                #try:
                    #print(r)
                    dialog_id = find_dialog(id_user,r[0], cur)
                    if dialog_id > 0:
                        message = get_last_message_chat(dialog_id,cur)
                        # message['id_chat'] = dialog_id
                        # message['name'] = r[1]
                    else:
                        message = {}
                    us = User(id_user=r[0], full_name=r[1], login=r[2])
                    data = {
                        'users': [us.get_user()],
                        'message': message,
                        'id_chat': dialog_id,
                    }
                    res.append(data)
                #except:
                    #conn.close()
                    #conn = create_connection()
                    #cur = conn.cursor()
                    #traceback.print_exc()
                    #pass
            return res

        except:
            traceback.print_exc()
            return []
    return []


def find_user_in_contacts(name,
                          id_user,
                          page=0):
    select = '''
            SELECT DISTINCT (f.id_user),u.full_name, u.login, f.similarit
        FROM find('{}')f, users u
        WHERE u.id_user = f.id_user
        AND u.id_user in (
          SELECT ref_id_user
          FROM chat_to_user
          WHERE ref_id_chat IN (
            SELECT cu.ref_id_chat
            FROM chat_to_user cu
            WHERE cu.ref_id_user = {}
          )

        )
        AND u.id_user != {}
        ORDER BY similarit DESC, id_user DESC
        LIMIT {} OFFSET {}
        '''.format(name,
                   id_user,
                   id_user,
                   user_on_page,
                   user_on_page * int(page))

    conn = create_connection()
    print(select)
    if conn:
        try:
            cur = conn.cursor()
            cur.execute(select)
            res = []
            for r in cur.fetchall():
                try:
                    dialog_id = find_dialog(id_user, r[0], cur)
                    if dialog_id > 0:
                        message = get_last_message_chat(dialog_id, cur)
                        message['id_chat'] = dialog_id
                        message['name'] = r[1]
                    else:
                        message = {}
                    us = User(id_user=r[0], full_name=r[1], login=r[2])
                    data = {
                        'users': [us.get_user()],
                        'message': message,
                        'id_chat': dialog_id
                    }
                    res.append(data)
                except:
                    traceback.print_exc()
                    pass
            return res

        except:
            return []
    return []


def get_contacts(id_user,
                 page=0):
    select = '''
                SELECT id_user, full_name, login
                FROM users
                WHERE id_user IN (
                  SELECT ref_id_user
                  FROM chat_to_user, chat
                  WHERE ref_id_chat in (
                    SELECT ref_id_chat
                    FROM chat_to_user
                    WHERE ref_id_user = {}
                  )
                  AND ref_id_user  != {}
                  AND id_chat = ref_id_chat
                  AND is_chat = FALSE)
                  ORDER BY full_name ASC LIMIT {} OFFSET {}
        '''.format(
        id_user,
        id_user,
        user_on_page,
        user_on_page * int(page))
    # print(select)
    conn = create_connection()
    if conn:
        try:
            cur = conn.cursor()
            cur.execute(select)
            res = []
            for r in cur.fetchall():
                try:
                    dialog_id = find_dialog(id_user, r[0], cur)
                    if dialog_id > 0:
                        message = get_last_message_chat(dialog_id, cur)
                        message['id_chat'] = dialog_id
                        message['name'] = r[1]
                    else:
                        message = {}
                    us = User(id_user=r[0], full_name=r[1], login=r[2])
                    data = {
                        'users': [us.get_user()],
                        'id_chat': dialog_id,
                        'message': message
                    }
                    res.append(data)
                except:
                    traceback.print_exc()
                    pass
            return res

        except:
            return []
    return []


def get_last_message_chat_with_date(id_chat,
                                    cur):
    select = '''
        SELECT message, ref_id_user, chat_message.created_on, title_chat, is_chat,id_chat_message, type_mess
        FROM chat_message, chat
        WHERE id_chat_message = (
                                  SELECT max(id_chat_message)
                                  FROM chat_message
                                  WHERE ref_id_chat = {})
        AND id_chat = ref_id_chat
    '''.format(id_chat)
    selects = '''
        SELECT u.id_user, u.full_name
        FROM chat_to_user cu, users u
        WHERE cu.ref_id_chat = {}
        AND cu.ref_id_user = u.id_user
    '''.format(id_chat)
    try:
        cur.execute(select)
        row = cur.fetchone()
        cur.execute(selects)
        users = {int(r[0]): r[1] for r in cur.fetchall()}
        if row:
            data = {
                'id_chat': id_chat,
                'text_message': row[0],
                'id_sender': row[1],
                'date_message': str(row[2]),
                'title_chat': row[3],
                'name': row[3],
                'is_chat': row[4],
                'id_message': row[5],
                'is_deleted': row[6],
                'type_message': row[6],
            }

            return data, users
        return {
            'id_chat': id_chat,
            'is_chat': False,
            'date_message': str(datetime(year=1970, month=1, day=1)),
            'title_chat': 'hz'
        }, users

    except:
        traceback.print_exc()
        return {
            'id_chat': id_chat,
            'date_message': datetime(year=1970, month=1, day=1),
            'title_chat': 'hz'
        },{}


def get_quantity_message_chat_for_user(id_user,
                                       id_chat,
                                       cur):
    count = get_events_user_chat(id_user,id_chat)
    if count is not None:
        return count
    select = '''
        SELECT COUNT (id_new_message)
        FROM chat_new_message
        WHERE ref_id_chat = {}
        AND ref_id_user = {}
    '''.format(id_chat, id_user)
    cur.execute(select)
    count = cur.fetchone()[0]
    set_event_user_from_chat(id_user,
                             id_chat,
                             count)
    return count


def get_users_chat(id_chat,
                   cur):
    users = guc(id_chat)
    if users:
        return users
    select = '''
        SELECT ref_id_user
        FROM chat_to_user
        WHERE ref_id_chat = {}
    '''.format(id_chat)
    cur.execute(select)
    data = [r[0] for r in cur.fetchall()]
    add_users_to_chat(id_chat, data)
    return data


def get_information_user(id_user,
                         cur):
    select = '''
        SELECT u.id_user, u.full_name, u.login,is_male
        FROM users u
        WHERE u.id_user = {}
    '''.format(id_user)
    cur.execute(select)
    row = cur.fetchone()
    sel_ph = '''
                                select id_photo, path_img, created_on
                                from users_photo
                                WHERE ref_id_user = {}
                                ORDER BY created_on DESC
                            '''.format(id_user)
    cur.execute(sel_ph)
    pho = cur.fetchone()
    if pho:
        photo = [{
            'id_photo': pho[0],
            'path_img': str_path_to_file_nginx_gunicorn + str_path_to_photo_profile + pho[1],
            'created_on': str(pho[2])
        }]
    else:
        if row[3] is None:
            photo = [{
                'id_photo': -1,
                'path_img': str_path_to_file_nginx_gunicorn + str_path_to_photo_profile + '_default-inc.png',
                'created_on': '1970-01-01 00:00:00.000000'
            }]
        elif row[3]:
            photo = [{
                'id_photo': -1,
                'path_img': str_path_to_file_nginx_gunicorn + str_path_to_photo_profile + '_default-m.png',
                'created_on': '1970-01-01 00:00:00.000000'
            }]
        else:
            photo = [{
                'id_photo': -1,
                'path_img': str_path_to_file_nginx_gunicorn + str_path_to_photo_profile + '_default-f.png',
                'created_on': '1970-01-01 00:00:00.000000'
            }]
    return {
        'id_user': row[0],
        'role': 'user',
        'full_name': row[1],
        'login': row[2],
        'actual_photo': photo[0]
    }


def get_dialogs(id_user,
                page=0):
    select = '''
        SELECT ref_id_chat
        FROM chat_to_user
        WHERE ref_id_user = {}
    '''.format(id_user)
    # print(select)
    conn = create_connection()
    if conn:
        try:
            cur = conn.cursor()
            cur.execute(select)
            chats = []
            rows = cur.fetchall()
            for r in rows:
                chat_m, users_c = get_last_message_chat_with_date(r[0], cur)
                if users_c:
                    del users_c[int(id_user)]
                    if users_c and chat_m['is_chat'] is False:
                        chat_m['title_chat'] = users_c[list(users_c.keys())[0]]


                chats.append(chat_m)
            res = sorted(chats, key=lambda k: k['date_message'], reverse=True)
            answer = res[user_on_page * int(page): user_on_page * int(page) + user_on_page]
            ans = []
            for a in answer:
                chat_id = a['id_chat']
                titl_chat = a['title_chat']
                users = [get_information_user(r, cur) for r in get_users_chat(chat_id, cur)]
                ans.append({
                        'users': users,
                        'is_chat': a['is_chat'],
                        'title_chat': titl_chat,
                        'id_chat': chat_id,
                        'message': a,
                        'quantity_new_message': get_quantity_message_chat_for_user(id_user, chat_id, cur)
                    })
            # from pprint import pprint
            # pprint(ans)
            return ans
        except:
            traceback.print_exc()
            return []
    return []

