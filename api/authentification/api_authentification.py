
from flask import request, \
    jsonify
import traceback
from api.list_api import api_log_in,\
    api_check_valid_data_user,\
    api_registration,\
    api_log_out
from api._models.User import User
from api._api_answers import error_arguments,\
    error_server,\
    error_select,\
    error_connect,\
    not_valid_data_entry,\
    good_sign_in,\
    user_with_login_exist,\
    good_logout
from api.config_api import create_connection
from api.redis_db.redb import set_boss_multi,\
    set_user_multi,\
    check_valid_data_multi,\
    logout_multi

from api.__init__ import app


def log_in_():
    try:
        login = request.json['login']
        password = request.json['password']
        conn = create_connection()
        if conn:
            try:
                cur = conn.cursor()
                user = User(login=login, password_hash=password)
                ans = user.check_user(cur)
                cur.close()
                conn.close()
                if ans < 0:
                    app.logger.error('')
                    return jsonify({
                        'code': 1,
                        'message': not_valid_data_entry,
                        'data': {}
                    })
                else:
                    if user.role == 'user':
                        set_user_multi(user.id, user.session_key)
                    else:
                        set_boss_multi(user.id, user.session_key)
                    answer = jsonify({
                        'code': 1,
                        'message': good_sign_in,
                        'data': {
                            'user': user.get_user()
                        }
                    })
                    answer.headers.extend({
                        'Id-User': str(user.id),
                        'Session-Key': str(user.session_key),
                        'Role': str(user.role)
                    })
                    return answer
            except:
                conn.close()
                return jsonify({
                    'code': 0,
                    'message': error_select,
                    'data': {}
                })
        return jsonify({
            'code': 0,
            'message': error_connect,
            'data': {}
        })

    except KeyError:
        return jsonify({
            'code': 0,
            'message': error_arguments,
            'data': {}
        })
    except:
        traceback.print_exc()
        return jsonify({
            'code': 0,
            'message': error_server,
            'data': {}
        })


def registration():
    try:
        login = request.json['login']
        password = request.json['password']
        full_name = request.json['full_name']
        conn = create_connection()
        if conn:
            try:
                cur = conn.cursor()
                user = User(login=login,
                            password_hash=password,
                            full_name=full_name)
                ans = user.registration(cur)
                conn.commit()
                cur.close()
                conn.close()
                if ans < 0:
                    app.logger.error('not registration user with \n\tlogin: {}\n\tfull-name: {}'.format(login, full_name))
                    return jsonify({
                        'code': 1,
                        'message': user_with_login_exist,
                        'data': {}
                    })
                else:
                    if user.role == 'user':
                        set_user_multi(user.id, user.session_key)
                    else:
                        set_boss_multi(user.id, user.session_key)
                    answer = jsonify({
                        'code': 1,
                        'message': good_sign_in,
                        'data': {
                            'user': user.get_user()
                        }
                    })
                    answer.headers.extend({
                        'Id-User': str(user.id),
                        'Session-Key': str(user.session_key),
                        'Role': str(user.role)
                    })
                    return answer
            except:
                app.logger.error('registration error')
                conn.close()
                return jsonify({
                    'code': 0,
                    'message': error_select,
                    'data': {}
                })
        return jsonify({
            'code': 0,
            'message': error_connect,
            'data': {}
        })

    except KeyError:
        return jsonify({
            'code': 0,
            'message': error_arguments,
            'data': {}
        })
    except:
        traceback.print_exc()
        return jsonify({
            'code': 0,
            'message': error_server,
            'data': {}
        })


def check_valid_data_user():
    try:
        role = request.headers['Role']
        id_user = request.headers['Id-User']
        session_key = request.headers['Session-Key']
        return jsonify(
            {
                'answer': str(check_valid_data_multi(id_user,
                                                     session_key,
                                                     role))
            })
    except KeyError:
        return jsonify({
            'answer': str(False)
        })
    except:
        traceback.print_exc()
        return jsonify({
            'answer': str(False)
        })


def log_out():
    try:
        role = request.headers['Role']
        id_user = request.headers['Id-User']
        session_key = request.headers['Session-Key']
        logout_multi(id_user, session_key, role)
        return jsonify({
            'code': 1,
            'message': good_logout,
            'data': {}
        })
    except KeyError:
        return jsonify({
            'code': 0,
            'message': error_arguments,
            'data': {}
        })
    except:
        traceback.print_exc()
        return jsonify({
            'code': 0,
            'message': error_server,
            'data': {}
        })


app.add_url_rule(api_log_in,
                 'login',
                 log_in_,
                 methods=['POST'])
app.add_url_rule(api_registration,
                 'registration',
                 registration,
                 methods=['POST'])
app.add_url_rule(api_check_valid_data_user,
                 'check_valid_data_user',
                 check_valid_data_user,
                 methods=['GET'])
app.add_url_rule(api_log_out,
                 'log_out',
                 log_out,
                 methods=['GET'])
