import os
import sys
from aiohttp import web


sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))

from socket_server.my_socket import app
from flask_script import Manager, Server

manager = Manager(web.run_app(app, host='0.0.0.0', port=4996))


manager.add_command("runserver", Server(
    use_debugger=True,
    use_reloader=True,
    host='0.0.0.0',
    port='4996'
    )
)

if __name__ == "__main__":
    manager.run()
