from SPA_flask_popup_events.urls.config_url import domen_url

url_get_contacts = domen_url + '/contacts'
url_get_dialogs = domen_url + '/dialogs'
url_get_messages = domen_url + '/message'
url_create_dialog = domen_url + '/create_dialog'
url_create_chat = domen_url + '/create_chat'

