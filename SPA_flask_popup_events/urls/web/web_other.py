from SPA_flask_popup_events.urls.config_url import domen_url

url_index = domen_url + '/'
url_sign_in = domen_url + '/sign_in'
url_sign_out = domen_url + '/sign_out'
url_registration = domen_url + '/registration'
