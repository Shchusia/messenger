from SPA_flask_popup_events.urls.config_url import domen_url

domen = domen_url

url_index = domen + '/'
url_sign_in = domen + '/sign_in'
url_logout = domen + '/logout'
url_language = domen + '/language'

url_map_by_address = domen + '/map/lat_lan/by/address'
url_map_by_click = domen + '/map/address/by/lat_lan'

url_message = domen + '/message'

url_other = domen + '/other'

url_messenger = domen + '/messenger'

