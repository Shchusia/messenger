from SPA_flask_popup_events.urls.config_api import domen_api

api_find_all = domen_api + '/find/user/{}'
api_find_contacts = domen_api + '/find/contacts/{}'
