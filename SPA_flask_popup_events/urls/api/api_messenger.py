from SPA_flask_popup_events.urls.config_api import domen_api

api_get_dialogs = domen_api + '/dialogs/{}'
api_get_message_dialog = domen_api + '/messages/chat/{}'
api_create_dialog = domen_api + '/create/dialog'
api_get_contacts = domen_api + '/contacts/{}'
api_create_chat = domen_api + '/create/chat'