from SPA_flask_popup_events.urls.config_api import domen_api

api_login = domen_api + '/log_in'
api_logout = domen_api + '/log_out'
api_registration = domen_api + '/registration'

