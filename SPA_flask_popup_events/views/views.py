from SPA_flask_popup_events.__init__ import app
from flask import render_template
import requests
from SPA_flask_popup_events.urls.web.web_messenger import url_get_contacts,\
    url_get_dialogs,\
    url_get_messages,\
    url_create_dialog
from SPA_flask_popup_events.urls.web.web_find import url_find_all,\
    url_find_contacts

import SPA_flask_popup_events.views.views_user.views_map
import SPA_flask_popup_events.views.views_user.view_messenger
import SPA_flask_popup_events.views.views_user.views_sign_in
import SPA_flask_popup_events.views.views_user.view_find


def main_page():
    #return render_template('templates_user/layout.html',
    #                       url_mess=url_get_messages)
    '''return render_template('test_messanger.html',
                           url_mess=url_get_messages,
                           url_find_c=url_find_contacts,
                           url_find_a=url_find_all,
                           url_create_d=url_create_dialog)
    '''
    return render_template('templates_user/layout.html',
                           url_mess=url_get_messages,
                           url_find_c=url_find_contacts,
                           url_find_a=url_find_all,
                           url_create_d=url_create_dialog)



app.add_url_rule('/home', 'main_page', main_page)