from SPA_flask_popup_events.__init__ import app
from flask import render_template,\
    jsonify,\
    request
import requests

from SPA_flask_popup_events.urls.web.web_messenger import url_get_contacts,\
    url_get_dialogs,\
    url_get_messages,\
    url_create_dialog,\
    url_create_chat
from SPA_flask_popup_events.urls.api.api_messenger import api_get_dialogs,\
    api_get_message_dialog,\
    api_create_dialog,\
    api_get_contacts,\
    api_create_chat


def get_dialogs():
    # print('123')
    i = 0
    dialogs = []
    is_ok = True
    message = ''
    header = {
        'Session-Key': request.cookies.get('session_key'),
        'Id-User': request.cookies.get('id_user'),
        'Role': request.cookies.get('role')
    }
    while True:
        res = requests.get(api_get_dialogs.format(i), headers=header)
        if res.status_code == 200:
            res = res.json()
            if res['message']['code'] > 0:
                if res['data']['chats']:
                    for d in res['data']['chats']:
                        dialogs.append(d)
                else:
                    break
            else:
                is_ok = False
                message = res['message']['body']
                break
        else:
            message = 'проблемы с удаленым сервером'
        i += 1
    from pprint import pprint
    pprint(dialogs)
    return jsonify({
        'dialogs': dialogs,
        'message': message
    })


def get_messages(id_chat, page=0):
    h = {
        'Session-Key': request.cookies.get('session_key'),
        'Id-User': request.cookies.get('id_user'),
        'Role': request.cookies.get('role'),
        'Id-Chat': id_chat
    }
    res = requests.get(api_get_message_dialog.format(page), headers=h)
    messages = []
    message = ''
    if res.status_code == 200:
        res = res.json()
        if res['message']['code'] > 0:
            if res['data']['messages']:
                for m in res['data']['messages']:
                    m['text_message'] = m['text_message'].replace('\n', '<br>')
                    messages.append(m)
        else:
            message = res['message']['body']
    else:
        message = 'проблемы с удаленым сервером'
    from pprint import pprint
    pprint(message)
    return jsonify({
        'messages': messages,
        'message': message
    })


def create_dialog(id_user):
    header = {
        'Session-Key': request.cookies.get('session_key'),
        'Id-User': request.cookies.get('id_user'),
        'Role': request.cookies.get('role')
    }
    message = ''
    data = {}
    res = requests.post(api_create_dialog,headers=header,json={'id_user': id_user})
    if res.status_code == 200:
        res = res.json()
        if res['message']['code'] > 0:
            data = res['data']['chat']
        else:
            message = res['message']['body']
    else:
        message = 'неизвестная ошибка на удаленном сервере'
    return jsonify({
        'message': message,
        'data': data
    })


def get_contacts():
    i = 0
    header = {
        'Session-Key': request.cookies.get('session_key'),
        'Id-User': request.cookies.get('id_user'),
        'Role': request.cookies.get('role')
    }
    contacts = []
    message = ''
    while True:
        res = requests.get(api_get_contacts.format(i), headers=header)
        if res.status_code == 200:
            res = res.json()
            if res['message']['code'] > 0:
                if res['data']['users']:
                    for d in res['data']['users']:
                        contacts.append(d)
                else:
                    break
            else:
                is_ok = False
                message = res['message']['body']
                break
        else:
            message = 'проблемы с удаленым сервером'
        i += 1
    from pprint import pprint
    pprint(contacts)
    return jsonify({
        'contacts': contacts,
        'message': message
    })


def create_chat():
    di = dict(request.form)
    list_users = di['list_users'][0].split(',')
    title_chat = di['title_chat'][0]
    print(list_users)
    print(title_chat)
    header = {
        'Session-Key': request.cookies.get('session_key'),
        'Id-User': request.cookies.get('id_user'),
        'Role': request.cookies.get('role')
    }
    data = {
        'list_user': list_users,
        'title_chat': title_chat
    }
    message = ''
    resp = None
    res = requests.post(api_create_chat,
                        headers=header,
                        json=data)
    if res.status_code == 200:
        res = res.json()
        if res['message']['code'] > 0:
            resp = res['data']['chat']
        else:
            message = res['message']['body']
    else:
        message = 'проблемы с удаленым сервером'
    return jsonify({
        'message': message,
        'chat': resp
    })


app.add_url_rule(url_get_dialogs,
                 'dialogs',
                 get_dialogs,
                 methods=['GET'])
app.add_url_rule(url_get_messages + '/<id_chat>/<page>',
                 'get_messages',
                 get_messages,
                 methods=['GET'])
app.add_url_rule(url_create_dialog + '/<id_user>',
                 'create_dialog',
                 create_dialog,
                 methods=['GET'])
app.add_url_rule(url_get_contacts,
                 'get_contacts',
                 get_contacts,
                 methods=['GET'])
app.add_url_rule(url_create_chat,
                 'create_chat',
                 create_chat,
                 methods=['POST'])
