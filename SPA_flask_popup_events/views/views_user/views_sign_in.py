from SPA_flask_popup_events.__init__ import app
from flask import render_template,\
    request,\
    make_response,\
    redirect,\
    url_for

from SPA_flask_popup_events.urls.web.web_other import url_index,\
    url_sign_in, \
    url_sign_out, \
    url_registration
from SPA_flask_popup_events.urls.api.api_login import api_login,\
    api_logout,\
    api_registration
import hashlib
import requests


def sign_in():
    def create_hash_password(password):
        return hashlib.sha256(bytes(password, 'utf-8')).hexdigest()

    message = ''
    if request.method == 'POST':
        di = dict(request.form)
        login = di['login'][0]
        password = create_hash_password(di['password'][0])
        print(password)
        data = {
            'login': login,
            'password': password
        }
        req = requests.post(api_login, json=data)
        if req.status_code == 200:
            header = req.headers
            req = req.json()
            if req['message']['code'] > 0:
                req = req['data']['user']
                resp = make_response(redirect(url_for('main_page')))
                resp.set_cookie('role', header['Role'])
                resp.set_cookie('session_key', header['Session-Key'])
                resp.set_cookie('id_user', header['Id-User'])
                resp.set_cookie('login', req['login'])
                resp.set_cookie('full_name', req['full_name'])
                resp.set_cookie('photo', req['actual_photo']['path_img'])
                return resp
            else:
                message += req['message']['body']
        else:
            message += 'проблемы с удаленным сервером'
    return render_template('sign_in.html',
                           message=message,is_registration=False)


def sign_out():
    header = {
        'Session-Key': request.cookies.get('session_key'),
        'Id-User': request.cookies.get('id_user'),
        'Role': request.cookies.get('role')
    }
    res = requests.get(api_logout, headers=header)
    resp = make_response(redirect(url_for('index')))
    resp.set_cookie('session_key', 'None')
    resp.set_cookie('id_user', 'None')
    resp.set_cookie('role', 'None')
    resp.set_cookie('login', 'None')
    resp.set_cookie('full_name', 'None')
    resp.set_cookie('photo', 'None')
    return resp


def registration():
    def create_hash_password(password):
        return hashlib.sha256(bytes(password, 'utf-8')).hexdigest()

    message = ''
    if request.method == 'POST':
        di = dict(request.form)
        login = di['login'][0]
        password = create_hash_password(di['password'][0])
        # print(password)
        data = {
            'login': login,
            'password': password,
            'full_name': di['name'][0]
        }
        req = requests.post(api_registration, json=data)
        if req.status_code == 200:
            header = req.headers
            req = req.json()
            if req['message']['code'] > 0:
                req = req['data']['user']
                resp = make_response(redirect(url_for('main_page')))
                resp.set_cookie('role', header['Role'])
                resp.set_cookie('session_key', header['Session-Key'])
                resp.set_cookie('id_user', header['Id-User'])
                resp.set_cookie('login', req['login'])
                resp.set_cookie('full_name', req['full_name'])
                resp.set_cookie('photo', req['actual_photo']['path_img'])
                return resp
            else:
                message += req['message']['body']
        else:
            message += 'проблемы с удаленным сервером'
    return render_template('sign_in.html',
                           message=message,
                           is_registration=True)


app.add_url_rule(url_index, 'index', sign_in, methods=['POST', 'GET'])
app.add_url_rule(url_sign_in, 'sign_in', sign_in, methods=['POST', 'GET'])
app.add_url_rule(url_sign_out, 'sign_out', sign_out, methods=['GET'])
app.add_url_rule(url_registration, 'registration', registration, methods=['POST', 'GET'])