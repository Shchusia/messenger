from SPA_flask_popup_events.__init__ import app
from flask import render_template,\
    request,\
    jsonify
import requests
from SPA_flask_popup_events.urls.api.api_find import api_find_all,\
    api_find_contacts
from SPA_flask_popup_events.urls.web.web_find import url_find_all,\
    url_find_contacts


def find_all(page=0):
    header = {
        'Session-Key': request.cookies.get('session_key'),
        'Id-User': request.cookies.get('id_user'),
        'Role': request.cookies.get('role')
    }
    name = dict(request.form)['name'][0]
    message = ''
    users = []
    print(name)
    res = requests.post(api_find_all.format(page),
                        headers=header,
                        json={'name': name})
    if res.status_code == 200:
        res = res.json()
        if res['message']['code'] > 0:
            for d in res['data']['users']:
                users.append(d)
        else:
            message = 'ошибка на удаленом сервере\n'+ res['message']['body']
    else:
        message = 'ошибка на удаленом сервере'
    return jsonify({
        'message': message,
        'users': users
    })


def find_contacts(page=0):
    pass


app.add_url_rule(url_find_all + '/<page>', 'find_all', find_all, methods=['POST'])
app.add_url_rule(url_find_contacts + '/<page>', 'find_contacts', find_contacts, methods=['POST'])
