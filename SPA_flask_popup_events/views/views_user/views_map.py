from SPA_flask_popup_events.__init__ import app
from flask import render_template

from SPA_flask_popup_events.urls.web.web_map import url_map_by_click,\
    url_message,\
    url_other,\
    url_messenger


#def google_map_by_address():
#    return render_template('boss/google_maps.html')


def google_map_by_lat_lan():
    return render_template('templates_user/google_maps.html')


def message():
    arr = [i for i in range(10)]
    return render_template('templates_user/message.html', arr=arr)


def other():
    return render_template('templates_user/chat.html')


def messenger():
    return render_template('templates_user/messanger.html')

# app.add_url_rule(url_map_by_address, 'google_map', google_map_by_address)
app.add_url_rule(url_map_by_click, 'google_map', google_map_by_lat_lan)
app.add_url_rule(url_message, 'message', message)
app.add_url_rule(url_other, 'other', other)
app.add_url_rule(url_messenger, 'messenger', messenger)