from flask import Flask,\
    request,\
    render_template,\
    redirect,\
    url_for,\
    make_response
import requests
import hashlib
import traceback

app = Flask(__name__)


import SPA_flask_popup_events.views.views


@app.errorhandler(404)
def page_not_found(e):
    return render_template('404.html'), 404
