aiohttp==1.3.4
redis==2.10.5
Flask==0.12
requests==2.18.4
psycopg2==2.7.1
Pillow==4.3.0
celery==4.1.0
flask_script==2.0.6
pushjack==1.4.0
gevent_socketio==0.3.6
