import asyncio
from aiohttp import web
import socketio
from socket_server.redket import add_socket,\
    remove_socket,\
    get_sid_user,\
    disconnect_,\
    add_contacts,\
    get_contacts
from socket_server.db_for_socket import send_message_db,\
    entry_to_chat,\
    remove_message_user,\
    get_new_chat,\
    leave_from_chat,\
    get_contacts_user,\
    participant_in_dialogue,\
    entry_to_account
from api.redis_db.redb import check_valid_data_multi
from socket_server._socket_answer import error_arguments,\
    error_valid_data,\
    unknown_error,\
    message_not_send,\
    not_user_chat,\
    not_exist_message,\
    not_your_message
import traceback
import requests


sio = socketio.AsyncServer(async_mode='aiohttp')
app = web.Application()
sio.attach(app)



domen = 'http://127.0.0.1:69/my_message/v1/api'
api_celery_push_message = domen + '/push_message'

namespace = '/brandager'


@sio.on('entry', namespace=namespace)
async def entry(sid, message):
    # print('entry {}'.format(sid))
    session_key = message['session_key']
    id_user = message['id_user']
    role = message['role']
    r = check_valid_data_multi(id_user,
                               session_key,
                               role)
    message = 'в ваш аккаунт вошли с неизвестного устройства'
    # print(message)
    if r:
        list_part, code, data = entry_to_account(id_user, message)
        list_part.append(int(id_user))
        list_part = list(set(list_part))
        if code >= 1:
            for lp in list_part:
                sid_pa = get_sid_user(lp)
                if sid_pa:
                    for s in sid_pa:
                        await sio.emit('new_message', {'data': data},
                                       room=s,
                                       namespace=namespace)
                else:
                    try:
                        data_ = {
                            'message': data,
                            'id_user': lp
                        }
                        res = requests.post(api_celery_push_message,
                                            json=data_)
                        # print()
                        print(res)
                    except:
                        traceback.print_exc()
                    pass


@sio.on('conn', namespace=namespace)
async def connect(sid, message):
    try:
        add_socket(message['session_key'],
                   message['id_user'],
                   sid)
        res = get_sid_user(message['id_user'], sid)
        if len(res) == 0:
            contacts = get_contacts(message['id_user'])  # все контакты пользователя
            if len(contacts) == 0:
                contacts = get_contacts_user(message['id_user'])
                for c in contacts:
                    add_contacts(message['id_user'], c)
            for con in contacts:
                list_sids = get_sid_user(con)
                for l in list_sids:
                    await sio.emit(' online_status',
                                   {'data': {
                                       'id_user': message['id_user'],
                                       'is_online': True

                                   }
                                   },
                                   room=l,
                                   namespace=namespace)

    except KeyError:
        traceback.print_exc()
        await sio.emit('error',
                       {'data': error_arguments},
                       room=sid,
                       namespace=namespace)
    except:
        await sio.emit('error',
                       {'data': unknown_error},
                       room=sid,
                       namespace=namespace)


@sio.on('disc', namespace=namespace)
async def disconnect(sid, message):
    try:
        if message.get('sid',False):
            remove_socket(message['session-key'],
                          message['id_user'],
                          message['sid'])
        else:
            remove_socket(message['session-key'],
                          message['id_user'],
                          sid)
    except KeyError:
        await sio.emit('error',
                       {'data': error_arguments},
                       room=sid,
                       namespace=namespace)
    except:
        await sio.emit('error',
                       {'data': unknown_error},
                       room=sid,
                       namespace=namespace)


@sio.on('send_message', namespace=namespace)
async def send_message(sid, message):
    # print('hello')
    # print(message)

    try:
        session_key = message['session_key']
        id_user = message['id_user']
        role = message['role']
        # print(message)
        r = check_valid_data_multi(id_user,
                                   session_key,
                                   role)
        #print(r)
        if r:
            chat_id = message['message']['id_chat']
            message_text = message['message']['text_message']
            try:
                type_message = message['message']['type_message']
            except KeyError:
                type_message = 1
            #print(message_text)
            list_part, code, data = send_message_db(id_user, message_text, chat_id,type_message)
            # print(code)
            # print(list_part)
            # print(data)
            if code >= 1:
                for lp in list_part:
                    sid_pa = get_sid_user(lp)
                    if sid_pa:
                        for s in sid_pa:
                            await sio.emit('new_message', {'data': data},
                                           room=s,
                                           namespace=namespace)
                    else:
                        try:
                            res = requests.post(api_celery_push_message, json={
                                'message': data,
                                'id_user': lp
                            })
                            # print()
                            print(res)
                        except:
                            traceback.print_exc()
                        pass
                        # отправка пуша
                #list_sender = get_sid_user(id_user)
                #for s in list_sender:
                #    await sio.emit('new_message', {'data': data},
                #                   room=s,
                #                   namespace=namespace)
            else:
                if code == -1:
                    await sio.emit('error',
                                   {'data': message_not_send},
                                   room=sid,
                                   namespace=namespace)
                else:
                    await sio.emit('error',
                                   {'data': not_user_chat},
                                   room=sid,
                                   namespace=namespace)
        else:
            await sio.emit('error',
                           {'data': error_valid_data},
                           room=sid,
                           namespace=namespace)
    except KeyError:
        traceback.print_exc()
        await sio.emit('error',
                       {'data': error_arguments},
                       room=sid,
                       namespace=namespace)
    except:
        traceback.print_exc()
        await sio.emit('error',
                       {'data': unknown_error},
                       room=sid,
                       namespace=namespace)


@sio.on('connect', namespace=namespace)
async def test_connect(sid, environ, *args, **kwargs):
    print('connect user')
    print(environ)
    print(args)
    print(kwargs)
    await sio.emit('my response', {'data': 'Connected', 'count': 0},
                   room=sid,
                   namespace=namespace)


@sio.on('reconnect', namespace=namespace)
async def reconnect(sid, environ):
    print('reconnect')
    # print(environ)


@sio.on('add_to_chat', namespace=namespace)
async def add_to_chat(sid, message):
    try:
        session_key = message['session_key']
        id_user = message['id_user']
        role = message['role']
        id_chat = message['id_chat']
        users, title = get_new_chat(id_chat)
        r = check_valid_data_multi(id_user,
                                   session_key,
                                   role)
        # print(r)
        if r:
            for lp in users:
                sid_pa = get_sid_user(lp)
                for s in sid_pa:
                    await sio.emit('added_to_chat', {'data': {'id_chat': id_chat,
                                                              'title_chat': title}},
                                   room=s,
                                   namespace=namespace)
        else:
            await sio.emit('error',
                           {'data': error_valid_data},
                           room=sid,
                           namespace=namespace)
    except KeyError:
        traceback.print_exc()
        await sio.emit('error',
                       {'data': error_arguments},
                       room=sid,
                       namespace=namespace)
    except:
        traceback.print_exc()
        await sio.emit('error',
                       {'data': unknown_error},
                       room=sid,
                       namespace=namespace)


@sio.on('leave_chat', namespace=namespace)
async def leave_chat(sid, message):
    try:
        session_key = message['session_key']
        id_user = message['id_user']
        role = message['role']
        if check_valid_data_multi(id_user,
                                  session_key,
                                  role):
            chat_id = message['id_chat']
            code, users = leave_from_chat(id_user, chat_id)
            if code >= 1:
                for u in users.keys():
                    sid_pa = get_sid_user(u)
                    for s in sid_pa:
                        await sio.emit('left_chat', {'data': {
                            'id_chat': chat_id,
                            'id_user': id_user
                        }},
                                       room=s,
                                       namespace=namespace)
            else:
                if code == 0:
                   pass
                else:
                    await sio.emit('error',
                                   {'data': not_user_chat},
                                   room=sid,
                                   namespace=namespace)
        else:
            await sio.emit('error',
                           {'data': error_valid_data},
                           room=sid,
                           namespace=namespace)
    except KeyError:
        traceback.print_exc()
        await sio.emit('error',
                       {'data': error_arguments},
                       room=sid,
                       namespace=namespace)
    except:
        await sio.emit('error',
                       {'data': unknown_error},
                       room=sid,
                       namespace=namespace)


@sio.on('disconnect', namespace=namespace)
async def disconnect(sid):
    print('Client disconnected')
    id_user = disconnect_(sid)
    sids_user = get_sid_user(id_user)
    contacts = get_contacts(id_user)  # все контакты пользователя
    if len(contacts) == 0:
        contacts = get_contacts_user(id_user)
        for c in contacts:
            add_contacts(id_user, c)
    for con in contacts:
        list_sids = get_sid_user(con)
        for l in list_sids:
            await sio.emit('typing',
                           {'data': {
                               'id_chat': 0,
                               'id_user': id_user,
                               'is_typing': False
                           }},
                           room=l,
                           namespace=namespace)

    if len(sids_user) == 0:
        contacts = get_contacts(id_user)  # все контакты пользователя
        if len(contacts) == 0:
            contacts = get_contacts_user(id_user)
            for c in contacts:
                add_contacts(id_user, c)
        for con in contacts:
            list_sids = get_sid_user(con)
            for l in list_sids:
                await sio.emit(' online_status',
                               {'data': {
                                   'id_user': id_user,
                                   'is_online': False
                               }
                               },
                               room=l,
                               namespace=namespace)


@sio.on('entry_chat', namespace=namespace)
async def entry_chat(sid, message):
    # print('entry')
    try:
        session_key = message['session_key']
        id_user = message['id_user']
        role = message['role']
        if check_valid_data_multi(id_user,
                                  session_key,
                                  role):
            chat_id = message['id_chat']
            id_last_sender = entry_to_chat(chat_id,
                                           id_user)
            # print(id_last_sender)
            if id_last_sender >= 1:
                sid_pa = get_sid_user(id_last_sender)
                for s in sid_pa:
                    await sio.emit('viewed_dialog', {'data': {
                        'id_chat': chat_id,
                        'id_user': id_user}
                    },
                                   room=s,
                                   namespace=namespace)
                list_sender = get_sid_user(id_user)
                for s in list_sender:
                    await sio.emit('viewed_dialog', {'data': {
                        'id_chat': chat_id,
                        'id_user': id_user}
                    },
                                   room=s,
                                   namespace=namespace)
            else:
                # print('error entry')
                if id_last_sender == 0:
                   pass
                else:
                    await sio.emit('error',
                                   {'data': not_user_chat},
                                   room=sid,
                                   namespace=namespace)
        else:
            # print('valid_data_entry')
            await sio.emit('error',
                           {'data': error_valid_data},
                           room=sid,
                           namespace=namespace)
    except KeyError:
        traceback.print_exc()
        await sio.emit('error',
                       {'data': error_arguments},
                       room=sid,
                       namespace=namespace)
    except:
        traceback.print_exc()
        await sio.emit('error',
                       {'data': unknown_error},
                       room=sid,
                       namespace=namespace)


@sio.on('remove_message', namespace=namespace)
async def remove_message(sid, message):
    # print('hello')
    try:
        session_key = message['session_key']
        id_user = message['id_user']
        role = message['role']
        # print(message)
        r = check_valid_data_multi(id_user,
                                   session_key,
                                   role)
        # print(r)
        if r:
            chat_id = message['id_chat']
            message_id = message['id_message']
            # print(message_text)
            code, message, list_part = remove_message_user(id_user, chat_id, message_id )
            if code >= 1:
                for lp in list_part:
                    sid_pa = get_sid_user(lp)
                    for s in sid_pa:
                        await sio.emit('deleted_message', {'data': message},
                                       room=s,
                                       namespace=namespace)
                list_sender = get_sid_user(id_user)
                for s in list_sender:
                    await sio.emit('deleted_message', {'data': message},
                                   room=s,
                                   namespace=namespace)
            else:
                if code == -1:
                    await sio.emit('error',
                                   {'data': not_user_chat},
                                   room=sid,
                                   namespace=namespace)
                elif code == -3:
                    await sio.emit('error',
                                   {'data': not_your_message},
                                   room=sid,
                                   namespace=namespace)
                else:
                    await sio.emit('error',
                                   {'data': not_exist_message},
                                   room=sid,
                                   namespace=namespace)
        else:
            await sio.emit('error',
                           {'data': error_valid_data},
                           room=sid,
                           namespace=namespace)
    except KeyError:
        traceback.print_exc()
        await sio.emit('error',
                       {'data': error_arguments},
                       room=sid,
                       namespace=namespace)
    except:
        traceback.print_exc()
        await sio.emit('error',
                       {'data': unknown_error},
                       room=sid,
                       namespace=namespace)


@sio.on('add_to_chat_new_user', namespace=namespace)
async def add_to_chat_new_user(sid, message):
    try:
        session_key = message['session_key']
        id_user = message['id_user']
        role = message['role']
        id_chat = message['id_chat']
        id_user_new = message['id_user_new']
        id_user_who = message['id_user_who']
        users, title = get_new_chat(id_chat)
        r = check_valid_data_multi(id_user,
                                   session_key,
                                   role)
        # print(r)
        if r:
            for lp in users:
                sid_pa = get_sid_user(lp)
                for s in sid_pa:
                    await sio.emit('new_user_chat', {'data': {'id_chat': id_chat,
                                                              'title_chat': title,
                                                              'id_user_new': id_user_new,
                                                              'id_user_who': id_user_who}},
                                   room=s,
                                   namespace=namespace)
        else:
            await sio.emit('error',
                           {'data': error_valid_data},
                           room=sid,
                           namespace=namespace)
    except KeyError:
        traceback.print_exc()
        await sio.emit('error',
                       {'data': error_arguments},
                       room=sid,
                       namespace=namespace)
    except:
        traceback.print_exc()
        await sio.emit('error',
                       {'data': unknown_error},
                       room=sid,
                       namespace=namespace)


@sio.on('send_file', namespace=namespace)
async def send_file(sid, message):
    try:
        list_part = message['list_part']
        data = message['message']
        code = 1
        if code >= 1:
            for lp in list_part:
                sid_pa = get_sid_user(lp)
                for s in sid_pa:
                    await sio.emit('new_message', {'data': data},
                                   room=s,
                                   namespace=namespace)
        else:
            if code == -1:
                await sio.emit('error',
                               {'data': message_not_send},
                               room=sid,
                               namespace=namespace)
            else:
                await sio.emit('error',
                               {'data': not_user_chat},
                               room=sid,
                               namespace=namespace)
    except KeyError:
        traceback.print_exc()
        await sio.emit('error',
                       {'data': error_arguments},
                       room=sid,
                       namespace=namespace)
    except:
        traceback.print_exc()
        await sio.emit('error',
                       {'data': unknown_error},
                       room=sid,
                       namespace=namespace)


@sio.on('is_typing', namespace=namespace)
async def is_typing(sid, message):
    # print('typing')
    try:
        session_key = message['session_key']
        id_user = message['id_user']
        role = message['role']
        r = check_valid_data_multi(id_user,
                                   session_key,
                                   role)
        # print(r)
        if r:
            chat_id = message['id_chat']
            # is_typin =
            # print('is typing: {}'.format(is_typin))
            # print(message_text)
            list_part = participant_in_dialogue(chat_id,id_user)

            list_part.append(id_user)
            # print(list_part)
            for lp in list_part:
                sid_pa = get_sid_user(lp)
                for s in sid_pa:
                    await sio.emit('typing', {'data': {
                        'id_chat': chat_id,
                        'id_user': id_user,
                        'is_typing': bool(message['is_typing'])
                    }},
                                   room=s,
                                   namespace=namespace)

        else:
            # print('valid_data_typing')
            await sio.emit('error',
                           {'data': error_valid_data},
                           room=sid,
                           namespace=namespace)
    except KeyError:
        traceback.print_exc()
        await sio.emit('error',
                       {'data': error_arguments},
                       room=sid,
                       namespace=namespace)
    except:
        traceback.print_exc()
        await sio.emit('error',
                       {'data': unknown_error},
                       room=sid,
                       namespace=namespace)





