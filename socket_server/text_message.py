user_leave_chat = 'Пользователь {} покинул(а/о) чат'
user_add_to_chat = 'Пользователь {} добавил(а/о) пользователя {} в чат'
message_delete = 'Сообщение удалено'
create_chat = '{} создал(а/о) чат "{}"'
create_dialog = '{} создал(а/о) диалог'
