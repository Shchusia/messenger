# -5 -6-7

error_valid_data = {
    'code': -5,
    'title': 'ошибка',
    'body': 'ваши данные не валидны'
}

message_not_send = {
    'code': -6,
    'title': 'ошибка',
    'body': 'сообщение не добавлено'
}

not_user_chat = {
    'code': -7,
    'title': 'ошибка',
    'body': 'вы не имеете отношения к данному чату'
}

error_arguments = {
    'code': -8,
    'title': 'ошибка',
    'body': 'нет необходимых данных для запроса'
}

unknown_error = {
    'code': -9,
    'title': 'ошибка',
    'body': 'неизвестная ошибка'
}

not_exist_message = {
    'code': -10,
    'title': 'ошибка',
    'body': 'нет сообщения в переписке'
}

not_your_message = {
    'code': -11,
    'title': 'ошибка',
    'body': 'вы не можете удалить не ваше сообщение'
}
