from socket_server.config_api import create_connection
import traceback
from socket_server.text_message import user_leave_chat,\
    message_delete
from socket_server.redis_cache import is_exist_user_in_chat,\
    add_users_to_chat,\
    add_user_to_chat,\
    get_name_user as gnu,\
    set_name_user,\
    set_name_chat,\
    get_name_chat,\
    get_users_chat,\
    set_last_sender_chat,\
    get_last_sender_chat,\
    add_event_user,\
    set_event_user_from_chat,\
    remove_user_from_chat


def is_exist_user_to_chat(id_chat,
                          id_user,
                          cur):
    if is_exist_user_in_chat(id_chat, id_user):
        return True
    select = '''
        SELECT chat_to_user.get_notification
        FROM chat_to_user
        WHERE ref_id_chat = {}
        AND ref_id_user = {}
    '''.format(id_chat, id_user)
    cur.execute(select)
    try:
        r = cur.fetchone()[0]
        if r is not None:
            add_user_to_chat(id_chat,
                             id_user)
            return True
        return False
    except:
        return False


def is_exist_message_in_chat(id_chat,
                             id_message,
                             cur):

    '''
    навернуть кэш потом
    :param id_chat:
    :param id_message:
    :param cur:
    :return:
    '''
    select = '''
            SELECT id_chat_message
            FRom chat_message
            WHERE id_chat_message = {}
            AND ref_id_chat = {}
        '''.format(id_message, id_chat)
    cur.execute(select)
    try:
        row = cur.fetchone()
        if row[0] is not None:
            return True
        else:
            return False
    except:
        return False


def get_name_user(id_user,
                  cur):
    name = gnu(id_user)
    if name is not None:
        return name
    select = '''
        SELECT full_name
        FROM users
        WHERE id_user = {}
    '''.format(id_user)
    cur.execute(select)
    name = cur.fetchone()[0]
    set_name_user(id_user, name)
    return name


def get_inf_chat(id_chat,
                 cur):
    title_chat = get_name_chat(id_chat)
    if title_chat:
        return id_chat, title_chat
    select = '''
        SELECT is_chat, title_chat
        FROM chat
        WHERE id_chat = {}
    '''.format(id_chat)
    cur.execute(select)
    row = cur.fetchone()
    set_name_chat(id_chat,
                  row[1])
    return row[0], row[1]


def participant_in_dialogue(id_chat,
                            id_user,
                            cur=None):
    users = get_users_chat(id_chat)
    if users:
        return users
    select = '''
        SELECT ref_id_user
        FROM chat_to_user
        WHERE ref_id_user != {}
        AND ref_id_chat = {}
    '''.format(id_user, id_chat)
    if cur is None:
        conn = create_connection()
        cur = conn.cursor()
        cur.execute(select)
        data = [r[0] for r in cur.fetchall()]
        d = data
        d.append(id_user)
        add_users_to_chat(id_chat, d)
        return data

    else:
        cur.execute(select)
        data = [r[0] for r in cur.fetchall()]
        d = data
        d.append(id_user)
        add_users_to_chat(id_chat, d)
        return data


def send_message_db(user_id,
                    message,
                    id_chat,
                    type_message=1):
    conn = create_connection()
    if conn:
        cur = conn.cursor()
        is_ex = is_exist_user_to_chat(id_chat, user_id, cur)
        if is_ex:  # проверка отношения пользователя к чату
            insert_message = '''
                INSERT INTO chat_message (id_chat_message, message, ref_id_chat, ref_id_user, created_on,type_mess)
                VALUES (DEFAULT, '{}', {},{}, DEFAULT,{}) Returning created_on, id_chat_message
            '''.format(message.replace("'", "\'"), id_chat,
                       user_id,
                       type_message)
            try:
                cur.execute(insert_message)
                conn.commit()
                row = cur.fetchone()
                date = row[0]
                id_mess = row[1]
                parti_dia = participant_in_dialogue(id_chat, user_id, cur)
                for p in parti_dia:
                    if int(p) != int(user_id):
                        select = '''
                            INSERT INTO chat_new_message (id_new_message, ref_id_user, ref_id_chat)
                            VALUES (DEFAULT , {}, {} )
                        '''.format(p, id_chat)
                        cur.execute(select)
                        conn.commit()
                        add_event_user(p,
                                       id_chat)
                set_last_sender_chat(id_chat,
                                     user_id)
                name = get_name_user(user_id,
                                     cur)
                is_chat, title_chat = get_inf_chat(id_chat,
                                                   cur)
                conn.close()
                return parti_dia, 1, {
                    'text_message': message,
                    'name': name,
                    'id_chat': id_chat,
                    'id_sender': user_id,
                    'date_message': str(date),
                    'is_chat': is_chat,
                    'title_chat': title_chat,
                    'is_deleted': False,
                    'type_message': type_message,
                    'id_message': id_mess
                }
            except:
                traceback.print_exc()
                return [], -1, 0  # 'сообщение не добавлено'
            pass
        else:
            return [], -2, 0  # 'не имеешь отношения к чату'


def get_id_lsat_sender_chat(id_chat,
                            cur):
    id_last_sender = get_last_sender_chat(id_chat)
    if id_last_sender:
        return id_last_sender
    select = '''
        SELECT ref_id_user
        FROM chat_message, chat
        WHERE id_chat_message = (
                                  SELECT max(id_chat_message)
                                  FROM chat_message
                                  WHERE ref_id_chat = {})
        AND id_chat = ref_id_chat
    '''.format(id_chat)

    try:
        cur.execute(select)
        row = cur.fetchone()
        if row[0]:
            set_last_sender_chat(id_chat,
                                 row[0])
            return row[0]
        return 0
    except:
        # traceback.print_exc()
        return 0


def entry_to_chat(id_chat,
                  id_user):
    conn = create_connection()
    if conn:
        cur = conn.cursor()
        is_ex = is_exist_user_to_chat(id_chat,
                                      id_user,
                                      cur)
        if is_ex and get_id_lsat_sender_chat(id_chat,
                                             cur) > 0:  # проверка отношения пользователя к чату

               select = '''
                    DELETE FROM chat_new_message WHERE ref_id_chat = {} AND ref_id_user = {}
               '''.format(id_chat, id_user)
               cur.execute(select)
               conn.commit()
               conn.close()
               set_event_user_from_chat(id_user,
                                        id_chat,
                                        0)
               return get_id_lsat_sender_chat(id_chat,
                                              cur)

        elif not is_ex:
            return 0
        else:
            return -1  # 'не имеешь отношения к чату


def get_message(id_message,
                cur):
    select = '''
        SELECT cm.id_chat_message,cm.message,cm.ref_id_user, cm.ref_id_chat,cm.created_on, u.full_name,c.is_chat,c.title_chat
        FROM chat_message cm, chat c, users u
        WHERE cm.id_chat_message = {}
        AND cm.ref_id_chat = c.id_chat
        AND cm.ref_id_user = u.id_user

    '''.format(id_message)
    cur.execute(select)
    row = cur.fetchone()
    return {
        'text_message': row[1],
        'name': row[5],
        'id_chat': row[3],
        'id_sender': row[2],
        'date_message': str(row[4]),
        'is_chat': row[6],
        'title_chat': row[7],
        'id_message': row[0]
    }


def remove_message_user(id_user,
                        id_chat,
                        id_message):
    conn = create_connection()
    if conn:
        cur = conn.cursor()
        is_ex = is_exist_user_to_chat(id_chat, id_user, cur)
        if is_ex:  # проверка отношения пользователя к чату
            if is_exist_message_in_chat(id_chat, id_message, cur):
                select = '''
                    UPDATE chat_message
                    SET type_mess = 2,
                    message = '{}'
                    WHERE id_chat_message = {}
                    AND ref_id_chat = {}
                    AND ref_id_user = {}
                    RETURNING id_chat_message
                '''.format(message_delete,id_message,id_chat, id_user)
                cur.execute(select)
                rows = cur.fetchall()
                try:
                    message = get_message(id_message, cur)
                    parti_dia = participant_in_dialogue(id_chat, id_user, cur)
                    if rows:
                        conn.commit()
                        conn.close()
                        return 1, message, parti_dia    # удалено
                    return -3, {}, []
                except TypeError:
                    return -3, {}, [] # это не ваше сообщение чтоб удалять его
                except:
                    return -3, {}, []
            else:
                return -2, {}, []  # нет такого сообщения
        else:
            return -1, {}, []  # 'не имеешь отношения к чату


def get_new_chat(id_chat):
    conn = create_connection()
    if conn:
        try:
            cur = conn.cursor()
            select = '''
                SELECT ref_id_user
                FROM chat_to_user
                WHERE ref_id_chat = {}
            '''.format(id_chat)
            cur.execute(select)
            users = cur.fetchall()
            select = '''
                SELECT title_chat
                FROM chat
                WHERE id_chat = {}
            '''.format(id_chat)
            cur.execute(select)
            conn.close()
            title_chat = cur.fetchone()
            return users, title_chat
        except:
            traceback.print_exc()
    return [], ''


def leave_from_chat(id_user,
                    id_chat):
    conn = create_connection()
    if conn:
        cur = conn.cursor()

        select = '''
            SELECT ref_id_user
            FROM chat_to_user
            WHERE ref_id_chat = {}
        '''.format(id_chat)
        cur.execute(select)
        rows = {r[0]: r[0] for r in cur.fetchall()}
        if rows.get(int(id_user), False):
            select = '''
                DELETE FROM chat_to_user WHERE ref_id_user = {} AND ref_id_chat = {}
            '''.format(id_user, id_chat)
            cur.execute(select)
            remove_user_from_chat(id_chat,
                                  id_user)
            mess = user_leave_chat.format(get_name_user(id_user,cur))
            insert = '''
                INSERT INTO chat_message (id_chat_message, message, ref_id_chat, ref_id_user, created_on, type_mess)
                VALUES (DEFAULT, '{}', {}, {}, DEFAULT, 6)
            '''.format(mess, id_chat, id_user)
            cur.execute(insert)
            conn.commit()
            conn.close()
            return 1, rows
        else:
            return -1, {}  # вы не имеете отношения к чату


def get_contacts_user(id_user):
    select = '''
        SELECT DISTINCT ct.ref_id_user
        FROM chat_to_user ct
        WHERE ct.ref_id_chat in (
                SELECT c.ref_id_chat
                FROM chat_to_user c
                WHERE c.ref_id_user = {})
        AND ct.ref_id_user != {}
    '''.format(id_user, id_user)
    conn = create_connection()
    res = []
    if conn:
        cur = conn.cursor()
        cur.execute(select)
        res = [r[0] for r in cur.fetchall()]
        conn.close()
    return res


def get_chats(id_user):
    conn = create_connection()
    if conn:
        cur = conn.cursor()
        select = '''
            SELECT ref_id_chat
        '''


def entry_to_account(id_user, message):
    def get_id_chat(cur):
        # conn = create_connection()
        # cur = conn.cursor()
        select = '''
            select DISTINCT c.ref_id_chat
            FROM chat_to_user  c, chat ch
            WHERE c.ref_id_chat in (SELECT ctu.ref_id_chat FROM chat_to_user ctu WHERE ctu.ref_id_user = {})
            and  c.ref_id_chat in (SELECT ct.ref_id_chat FROM chat_to_user ct WHERE ct.ref_id_user = {})
            and c.ref_id_chat = ch.id_chat
            and ch.is_chat = FALSE
        '''.format(id_user, 1)
        cur.execute(select)
        id_chat = cur.fetchone()[0]
        # conn.close()
        return id_chat

    conn = create_connection()
    if conn:
        cur = conn.cursor()
        id_chat = get_id_chat(cur)
        conn.close()
        return send_message_db(1,
                               message,
                               id_chat)




