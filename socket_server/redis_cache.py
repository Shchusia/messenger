import redis
import traceback

r = redis.StrictRedis(host='localhost', port=6379, db=13)


def add_users_to_chat(id_chat, users):
    for u in users:
        add_user_to_chat(id_chat,
                         u)


def add_user_to_chat(id_chat,
                     id_user):
    r.lpush('chat_{}'.format(id_chat),
            id_user)


def remove_user_from_chat(id_chat,
                          id_user):
    try:
        r.lrem('chat_{}'.format(id_chat),
               1,
               id_user)
    except:
        pass


def get_users_chat(id_chat):
    try:
        return [int(li.decode('utf-8')) for li in list(r.lrange('chat_{}'.format(id_chat), 0, -1))]
    except:
        return None


def is_exist_user_in_chat(id_chat,
                          id_user):
    try:
        l = list(r.lrange('chat_{}'.format(id_chat), 0, -1))
        # print(len(l))
        # print('___')
        for li in l:
            # print(li)
            if int(li.decode('utf-8')) == int(id_user):
                return True
        return False
    except:
        traceback.print_exc()
    return False


r_u = redis.StrictRedis(host='localhost', port=6379, db=12)


def get_name_user(id_user):
    try:
        return r_u.get('name_{}'.format(id_user)).decode('utf-8')
    except:
        return None


def set_name_user(id_user, name):
    r_u.set('name_{}'.format(id_user), name)


def get_name_chat(id_chat):
    try:
        return r_u.get('chat_{}'.format(id_chat)).decode('utf-8')
    except:
        return None


def set_name_chat(id_chat, name):
    r_u.set('chat_{}'.format(id_chat), name)


def set_last_sender_chat(id_chat, id_sender):
    r_u.set('chat_sender_{}'.format(id_chat), id_sender)


def get_last_sender_chat(id_chat):
    try:
        return int(r_u.get('chat_sender_{}'.format(id_chat)).decode('utf-8'))
    except:
        return None


def set_event_user_from_chat(id_user,
                             id_chat,
                             count_event):
    r_u.set('event_{}_chat_{}'.format(id_user,id_chat),
            count_event)


def add_event_user(id_user,
                   id_chat):
    try:
        events = int(r_u.get('event_{}_chat_{}'.format(id_user,id_chat)).decode('utf-8'))
    except:
        events = 0
    events += 1
    set_event_user_from_chat(id_user,
                             id_chat,
                             events)


def get_events_user_chat(id_user, id_chat):
    try:
        return int(r_u.get('event_{}_chat_{}'.format(id_user,id_chat)).decode('utf-8'))
    except:
        return None



