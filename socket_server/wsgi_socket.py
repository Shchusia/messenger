import os, sys
# from aiohttp import web

sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))

from aiohttp import web
from socket_server.my_socket import app
# from socket_server.my_socket import app as ap
web.run_app(app, host='0.0.0.0', port=4995)
if __name__ == "__main__":
        pass
        # web.run_app(ap)
