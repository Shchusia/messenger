import redis


ttl_phone_in_redis = 300

r = redis.StrictRedis(host='localhost', port=6379, db=15)


def add_socket(session_key, id_user, sid):
    # r.set('{}_{}'.format(session_key, id_user), sid)
    r.lpush('{}'.format(id_user), sid)
    r.set(sid, id_user)


def remove_socket(session_key, id_user, sid):
        try:
            r.lrem('{}'.format(id_user), 1, sid)
            # r.delete('{}_{}'.format(session_key, id_user))
        except:
            pass


def disconnect_(sid):
    try:
        id_us = r.get(sid).decode('utf-8')
        r.lrem('{}'.format(id_us), 1, sid)
        r.delete(sid)
        return id_us
    except:
        return 0


def get_sid_user(id_user, sid=None):
    l = list(r.lrange('{}'.format(id_user), 0, -1))
    res = []
    if sid:
        for li in l:
            if li.decode('utf-8') != sid:
                res.append(li.decode('utf-8'))
    else:
        for li in l:
            res.append(li.decode('utf-8'))
    return res


r_c = redis.StrictRedis(host='localhost', port=6379, db=14)  # для хранения контактов


def add_contacts(id_user, him_contacts):
    r_c.lpush(id_user, him_contacts)


def rem_contacts(id_user, who_delete):
    try:
        r.lrem(id_user, 1, who_delete)
    except:
        pass


def get_contacts(id_user):
    try:
        l = list(set(r.lrange(id_user, 0, -1)))
        return l
    except:
        return []
