CREATE SEQUENCE push_data_id START 1;
CREATE SEQUENCE photo_id START 1;
CREATE SEQUENCE type_message_id START 1;
CREATE SEQUENCE user_id START 1;
CREATE SEQUENCE dialog_id START 1;
CREATE SEQUENCE dialog_message_id START 1;
CREATE SEQUENCE chat_id START 1;
CREATE SEQUENCE chat_message_id START 1;
CREATE SEQUENCE chat_impotent_message_id START 1;




CREATE TABLE users (
id_user INT PRIMARY KEY DEFAULT nextval('user_id'),
full_name VARCHAR(100),
password_hash VARCHAR(100),
login VARCHAR(100) UNIQUE,
birthday DATE,
phone VARCHAR(20) DEFAULT '',
email VARCHAR(100) DEFAULT '',
is_male BOOL,
about_me VARCHAR(500) DEFAULT '');

CREATE TABLE chat(
id_chat INT PRIMARY KEY DEFAULT nextval('chat_id'),
title_chat VARCHAR(200),
created_on timestamp without time zone DEFAULT timezone('utc'::text, now()),
is_chat BOOL DEFAULT FALSE

);

CREATE TABLE chat_message(
id_chat_message INT PRIMARY KEY DEFAULT nextval('chat_message_id'),
message TEXT,
ref_id_chat INT REFERENCES chat(id_chat),
ref_id_user INT REFERENCES users(id_user),
type_mess INT REFERENCES type_message(id_type_message),
created_on timestamp without time zone DEFAULT timezone('utc'::text, now())
);
CREATE TABLE chat_important_message(
id_chat_important_message INT DEFAULT nextval('chat_message_id'),
message TEXT,
ref_id_chat INT REFERENCES chat(id_chat),
ref_id_user INT REFERENCES users(id_user),
created_on timestamp without time zone DEFAULT timezone('utc'::text, now()),
ref_id_chat_message INT REFERENCES chat_message(id_chat_message),
ref_id_user_for_who_imp INT REFERENCES  users(id_user),
PRIMARY KEY (ref_id_chat_message, ref_id_user_for_who_imp)
);
CREATE TABLE chat_to_user(
ref_id_chat INT REFERENCES chat(id_chat),
ref_id_user INT REFERENCES users(id_user),
get_notification BOOL DEFAULT TRUE,
PRIMARY KEY (ref_id_chat, ref_id_user)
);

CREATE TABLE users_photo(
id_photo INT PRIMARY KEY DEFAULT nextval('photo_id'),
path_img VARCHAR(200),
created_on timestamp without time zone DEFAULT timezone('utc'::text, now()),
ref_id_user INT REFERENCES users(id_user)
);

CREATE SEQUENCE new_message_id START 1;

CREATE TABLE chat_new_message(
id_new_message INT PRIMARY KEY DEFAULT nextval('new_message_id'),
ref_id_user INT REFERENCES users(id_user),
ref_id_chat INT REFERENCES chat(id_chat)
);

CREATE TABLE type_message(
id_type_message INT PRIMARY KEY DEFAULT nextval('type_message_id'),
type_ VARCHAR(100)
);



CREATE TABLE push_data(
  id_push_data INT PRIMARY KEY DEFAULT nextval('push_data_id'),
  device_id VARCHAR(100) UNIQUE ,
  push_key VARCHAR(100),
  is_ios BOOL,
  session_key VARCHAR(100),
  ref_id_user INT REFERENCES users (id_user)
);

CREATE OR REPLACE FUNCTION check_user(log VARCHAR(100), pass VARCHAR(100)) RETURNS INT
  LANGUAGE plpgsql
AS $$
  DECLARE id INT;
  BEGIN
    SELECT id_user INTO id
    FROM users
    WHERE login = $1
    AND password_hash = $2;
    IF id IS NOT NULL THEN
      RETURN id; -- id пользователя
    ELSE
      RETURN -1; --не верный логин или пароль
    END IF;
  END;
$$;

CREATE EXTENSION pg_trgm;
CREATE INDEX trgm_idx_login ON users USING GIN (login gin_trgm_ops);
CREATE INDEX trgm_idx_full_name ON users USING GIN (full_name gin_trgm_ops);

CREATE TYPE users_find as(
  id_user INT,
  --full_name VARCHAR(100),
  --login VARCHAR(100),
  similarit FLOAT
);

CREATE OR REPLACE FUNCTION find (nam VARCHAR(100)) RETURNS SETOF users_find AS
  $$
  DECLARE res users_find;
  BEGIN
    FOR res.id_user, res.similarit IN
      SELECT u.id_user, similarity(u.login, $1) AS sml
      FROM users u
      WHERE u.login % $1
      LOOP
        RETURN NEXT res;
      END LOOP;
    FOR res.id_user, res.similarit IN
      SELECT u.id_user, similarity(u.full_name, $1) AS sml
      FROM users u
      WHERE u.full_name % $1

      LOOP
        RETURN NEXT res;
      END LOOP;

    RETURN;

  END
  $$ LANGUAGE plpgsql;

SELECT set_limit(0.2);

INSERT INTO type_message (id_type_message, type_) VALUES (1, 'обычное');
INSERT INTO type_message (id_type_message, type_) VALUES (2, 'удалено');
INSERT INTO type_message (id_type_message, type_) VALUES (3, 'файл');
INSERT INTO type_message (id_type_message, type_) VALUES (5, 'добавился кто - то в чат');
INSERT INTO type_message (id_type_message, type_) VALUES (6, 'удалился кто - то из чата');
INSERT INTO type_message (id_type_message, type_) VALUES (4, 'пересланое');
INSERT INTO type_message (id_type_message, type_) VALUES (7, 'создание чата');
INSERT INTO type_message (id_type_message, type_) VALUES (8, 'создан диалог ');

/*
SELECT id_user, login, full_name, similarity(login, 'Андрhsdf') AS sml, similarity(full_name, 'Андрhsdf') AS smls
FROM users
WHERE login % 'Андрhsdf'
OR full_name % 'Андрhsdf';


SELECT DISTINCT (id_user), similarit
FROM find('osha6')
ORDER BY similarit DESC, id_user DESC;
*/