from pushjack import GCMClient,APNSClient
from config import key_android,\
    key_ios
import traceback


def push(GCMC, message, token_hex):
    resp = GCMC.send(token_hex,
                     #message,
                     message['title'],
                     notification=message,
                     #data=message,
                     # collapse_key='collapse_key',
                     delay_while_idle=True,
                     time_to_live=604800)


def push_android(GCMC, message, token_hex):
    new_message = {
        'header': message['title'],
        'message': message['body'],

    }
    del message['title']
    del message['body']
    new_message.update(message)
    resp = GCMC.send(token_hex,
                     new_message,
                     # message['title'],
                     # notification=message,
                     #data=message,
                     # collapse_key='collapse_key',
                     delay_while_idle=True,
                     time_to_live=604800)


def send_push(data_for_push, message):
    GCMC_android = GCMClient(api_key=key_android)
    GCMC_ios = GCMClient(api_key=key_ios)
    #print(help(APNSClient))
    #GCMC_ios = APNSClient(path_apns)
    for l in data_for_push:
        if not l[0]:
            # print(1)
            try:
                push_android(GCMC_android, message, l[1])
            except:
                GCMC_android = GCMClient(api_key=key_android)
                try:
                    push_android(GCMC_android, message, l[1])
                except:
                    GCMC_android = GCMClient(api_key=key_android)
        else:
            # print(2)
            try:
                # print(21)
                push(GCMC_ios, message, l[1])
                # print(23)
            except:
                traceback.print_exc()
                # print(24)
                GCMC_ios = GCMClient(api_key=key_ios)
                # print(25)
                #GCMC_ios = APNSClient(path_apns)

                try:
                    # print(26)
                    push(GCMC_ios, message, l[1])
                    # print(27)
                except:
                    # print(28)
                    GCMC_ios = GCMClient(api_key=key_ios)
                    # print(29)
                    #GCMC_ios = APNSClient(path_apns)
