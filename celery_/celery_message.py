from api.__init__ import celery
from config import str_connect_to_db
import psycopg2
import traceback
from celery_.pushs import send_push
from api.config_api import create_connection
from api.redis_db.redb import check_valid_data_multi


def get_notification_user(id_user,
                          id_chat,
                          cur):
    try:
        select = '''
            SELECT get_notification
            FROM chat_to_user
            WHERE ref_id_user = {}
            AND ref_id_chat = {}
        '''.format(id_user, id_chat)
        cur.execute(select)
        if cur.fetchone[0]:
            return True
        return False
    except:
        traceback.print_exc()
        return False


def get_push_data_user(id_user,
                       cur):
    select = '''
        SELECT is_ios, push_key, session_key
        FROM push_data
        WHERE ref_id_user = {}
    '''.format(id_user)
    cur.execute(select)
    return [[r[0], r[1], r[2]]for r in cur.fetchall]


@celery.task
def push_message(id_user, message):
    conn = create_connection()
    if conn:
        try:
            cur = conn.cursor()
            id_chat = message['id_chat']
            if get_notification_user(id_user,
                                     id_chat,
                                     cur):
                users_data = get_push_data_user(id_user, cur)
                for_push = [[u[0], u[1]] for u in users_data if check_valid_data_multi(id_user, u[2], 'user')]
                message = {
                    'title': message['name'],
                    'body': message['text_message'],
                    'id_chat': message['id_chat'],
                    'id_message': message['id_message'],
                    'date': str(message['date_message'])
                }
                send_push(for_push,
                          message)
        except:
            traceback.print_exc()
        conn.close()
